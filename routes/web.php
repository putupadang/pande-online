<?php

// use Symfony\Component\Routing\Route;
// use App\Http\Controllers\AdminController;
use Symfony\Component\HttpKernel\Tests\Controller;

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::match(['get', 'post'], '/status-order', 'HomeController@statusOrder');
Route::match(['get', 'post'], '/status-pembelian', 'HomeController@statusPembelian');
Route::match(['get', 'post'], '/invoice/{id}', 'HomeController@invoice');

//pemande (pelanggan view)
Route::get('/', 'IndexController@index');
Route::get('/list-pemande', 'IndexController@listPenjahit');
Route::match(['get', 'post'], '/pemande-detail/{id}', 'IndexController@detailPenjahit');

//galeri pemande
Route::get('/list-barang', 'IndexController@listKebaya');
Route::match(['get', 'post'], '/barang-detail/{id}', 'OrderController@detailKebaya');

//pemande register
Route::match(['get', 'post'], '/registrasi-pemande', 'IndexController@addPenjahit');

//order
Route::match(['get', 'post'], '/order/{id}', 'OrderController@addOrder');

//pembayaran
Route::get('/view-transaksi', 'TransaksiController@viewTransaksi');
Route::match(['get', 'post'], '/proses-transaksi/{id}', 'TransaksiController@prosesTransaksi');
Route::match(['get', 'post'], '/proses-transaksi-barang/{id}', 'TransaksiController@prosesTransaksiKebaya');

//batal order
Route::match(['get', 'post'], '/batal/{id}', 'OrderController@batal');
//batal beli
Route::match(['get', 'post'], '/batal-beli/{id}', 'OrderController@batalBeli');

//lihat pembayaran pelanggan
Route::get('/view-transaksi-login', 'TransaksiController@viewTransaksiLogin');

//pembayaran credit card
Route::match(['get', 'post'], '/credit-card/{id}', 'HomeController@creditCard');
Route::match(['get', 'post'], '/credit-card-kebaya/{id}', 'HomeController@creditCardKebaya');

//admin-login
Route::match(['get', 'post'], '/admin', 'AdminController@login');
// Route::get('admin', 'Auth\LoginController@showLoginForm')->name('login');
// Route::post('admin', 'Auth\LoginController@login');
Route::get('logout', 'AdminController@logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('admin/dashboard', 'AdminController@dashboard');
    Route::get('admin/setting', 'AdminController@setting');
    Route::get('admin/check-pwd', 'AdminController@chkPassword');
    Route::match(['get', 'post'], 'admin/update-pwd', 'AdminController@updatePwd');

    Route::get('/admin/view-pemande', 'AdminController@viewPenjahit');
    // Route::match(['get', 'post'], '/admin/add-penjahit', 'PenjahitController@addPenjahit');
    Route::match(['get', 'post'], '/admin/verifikasi-pemande/{id}', 'AdminController@verifikasiPenjahit');
    Route::match(['get', 'post'], '/admin/hapus-pemande/{id}', 'AdminController@delPemande');

    //route order(admin)
    Route::get('/admin/order-admin', 'OrderController@viewOrderAdmin');
    Route::match(['get', 'post'], '/admin/add-order', 'OrderController@addPenjahit');
    Route::get('/admin/order', 'OrderController@viewOrderKebayaAdmin');

    //route transaksi (admin)
    Route::get('/admin/view-transaksi', 'TransaksiController@viewTransaksiAdmin');
    Route::match(['get', 'post'], '/admin/refund-order/{id}', 'AdminController@refundOrder');
    Route::match(['get', 'post'], '/admin/verifikasi-order/{id}', 'AdminController@verifikasiPembayaran');
    Route::match(['get', 'post'], '/admin/setor-order-pesanan/{id}', 'AdminController@setorPesanan');
    //order kebaya
    Route::match(['get', 'post'], '/admin/refund-order-pembelian/{id}', 'AdminController@refundOrderKebaya');
    Route::match(['get', 'post'], '/admin/verifikasi-order-pembelian/{id}', 'AdminController@verifikasiPembayaranKebaya');
    Route::match(['get', 'post'], '/admin/setor-order-pembelian/{id}', 'AdminController@setorPembelian');
});

//-----Route Pemande-----//

//pemande login (pemande dan pelanggan)
Route::match(['get', 'post'], 'pemande', 'AuthPenjahit\LoginController@loginPenjahit');
Route::post('/logout-penjahit', 'AuthPenjahit\LoginController@logoutPenjahit');

//pemande page
Route::get('/pemande/home', 'PenjahitController@homePage');
Route::match(['get', 'post'], '/pemande/setting/{id}', 'PenjahitController@settings');

//route lihat order
Route::get('/pemande/view-order', 'PenjahitController@viewOrder');
Route::get('/pemande/view-order-pande', 'PenjahitController@viewOrderKebaya');

//terima order
Route::match(['get', 'post'], '/pemande/terima-order/{id}', 'PenjahitController@terimaOrder');
Route::match(['get', 'post'], '/pemande/selesai-order/{id}', 'PenjahitController@selesaiOrder');
Route::match(['get', 'post'], '/pemande/tolak-order/{id}', 'PenjahitController@tolakOrder');

//pembelian barang
Route::match(['get', 'post'], '/penjahit/terima-pembelian/{id}', 'PenjahitController@terimaPembelian');
Route::match(['get', 'post'], '/penjahit/tolak-pembelian/{id}', 'PenjahitController@tolakPembelian');

//galeri pemande
Route::match(['get', 'post'], '/pemande/tambah-galeri', 'PenjahitController@tambahGaleri');
Route::match(['get', 'post'], '/pemande/lihat-galeri', 'PenjahitController@lihatGaleri');
Route::match(['get', 'post'], '/pemande/edit-galeri/{id}', 'PenjahitController@editGaleri');
Route::match(['get', 'post'], '/pemande/hapus-galeri/{id}', 'PenjahitController@hapusGaleri');
