<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Mai Nyait</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="{{asset('css/creditcard_css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/creditcard_css/font-awesome.min.css')}}" />

    <script type="text/javascript" src="{{asset('js/creditcard_js/jquery-1.10.2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/creditcard_js/bootstrap.min.js')}}"></script>
</head>
<body>

<div class="container">

<div class="page-header">
    <h1><b>Pembayaran melalui Virtual Account</b></h1>
    <a href="/"><button class="btn btn-warning">HOME</button></a>
</div>

<!-- Credit Card Payment Form - START -->

@php
    $galeri = App\GaleriPenjahit::where('id', $order->id_galeri)->first();
    $penjahit = App\Penjahit::where('id', $galeri->id_penjahit)->first();
@endphp

{{-- detail order --}}
<div class="container col-md-8">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-center">Total yang Harus di Bayar : Rp.{{$galeri->harga}}</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pemilik Kebaya (Penjahit)</label>
                            <div class="input-group">
                                <span class="input-group-addon"></span>
                                <input type="text" class="form-control" value="{{$penjahit->nama}}" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nama Pemesan</label>
                            <div class="input-group">
                                <span class="input-group-addon"></span>
                                <input type="text" class="form-control" value="{{$order->nama_pembeli}}" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <div class="input-group">
                                <span class="input-group-addon"></span>
                                <input type="text" class="form-control" value="{{$order->alamat}}" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label>No. Hp</label>
                            <div class="input-group">
                                <span class="input-group-addon"></span>
                                <input type="text" class="form-control" value="{{$order->no_hp}}" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Pembelian</label>
                            <div class="input-group">
                                <span class="input-group-addon"></span>
                                <input type="text" class="form-control" value="{{$order->created_at}}" disabled />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>UKURAN BAJU</label>
                            <div class="input-group">
                                <span class="input-group-addon">panjang baju</span>
                                <input type="number" class="form-control" value="{{$galeri->panjang_baju}}" disabled />
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">lingkar dada</span>
                                <input type="number" class="form-control" value="{{$galeri->lingkar_dada}}" disabled />
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">lingkar pinggang</span>
                                <input type="number" class="form-control" value="{{$galeri->lingkar_pinggang}}" disabled />
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">lingkar pinggul</span>
                                <input type="number" class="form-control" value="{{$galeri->lingkar_pinggul}}" disabled />
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">lebar bahu</span>
                                <input type="number" class="form-control" value="{{$galeri->lebar_bahu}}" disabled />
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">panjang tangan</span>
                                <input type="number" class="form-control" value="{{$galeri->panjang_tangan}}" disabled />
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">lingkar lengan</span>
                                <input type="number" class="form-control" value="{{$galeri->lingkar_lengan}}" disabled />
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">lingkar tangan</span>
                                <input type="number" class="form-control" value="{{$galeri->lingkar_tangan}}" disabled />
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end detail order --}}

<div class="container col-md-4">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default">
                @if (session('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{session('flash_message_error')}}</strong>
                </div>
                @endif
                @if (session('flash_message_success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{session('flash_message_success')}}</strong>
                    </div>
                @endif
                <div class="panel-heading">
                    <div class="row">
                        <h3 class="text-center">Detail Pembayaran</h3>
                        <img class="img-responsive cc-img" src="{{asset('images/creditcard_images/creditcardicons.png')}}">
                    </div>
                </div>
                <form enctype="multipart/form-data" method="post" action="{{url('/credit-card-kebaya/'.$order->id)}}" novalidate="novalidate" name="add_credit_pay" id="add_credit_pay">
                {{ csrf_field() }}

                <input type="hidden" name="idOrder" id="idOrder" value="{{$order->id}}" />
                <input type="hidden" name="total" id="total" value="{{$galeri->harga}}" />
                
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>CARD NUMBER</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Valid Card Number" name="cardNumber"/>
                                        <span class="input-group-addon"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group">
                                    <label><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">EXP</span> Date</label>
                                    <input type="date" class="form-control" placeholder="MM / YY" name="expDate"/>
                                </div>
                            </div>
                            <div class="col-xs-5 col-md-5 pull-right">
                                <div class="form-group">
                                    <label>Kode CVC</label>
                                    <input type="number" class="form-control" placeholder="CVC" name="kodeCVC"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Pemilik Kartu</label>
                                    <input type="text" class="form-control" placeholder="Atas Nama" name="atasNama"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-warning btn-lg btn-block">Proses Pembayaran</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .cc-img {
        margin: 0 auto;
    }
</style>
<!-- Credit Card Payment Form - END -->

</div>

</body>
</html>