@extends('layouts.frontLayout.front_design')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    @if (session('flash_message_error'))
                    <div class="alert alert-error alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{session('flash_message_error')}}</strong>
                    </div>
                    @endif
                    @if (session('flash_message_success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{session('flash_message_success')}}</strong>
                        </div>
                    @endif
                    <h2>Masukkan Email Pemesan</h2>
                    <form enctype="multipart/form-data" method="get" action="{{url('/view-transaksi')}}" novalidate="novalidate" name="view_transaksi" id="view_transaksi" class="col-md-8">
                        {{ csrf_field() }}

                        <div class="control-group">
                            <label class="control-label">Email Pemesan</label>
                            <div class="controls">
                                <input type="email" name="emailPemesan" id="emailPemesan" placeholder="email">
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-default">Cari Transaksi</button>
                    </form>
                </div><!--/login form-->
            </div>
        </div>
    </div>
    <br><br>
@endsection