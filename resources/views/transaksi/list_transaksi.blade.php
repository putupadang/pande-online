@extends('layouts.frontLayout.front_design')

@section('content')
<div class="container">
        <div class="row">
            <div class="col-sm-12 padding-right">
                    @if (session('flash_message_error'))
                    <div class="alert alert-error alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{session('flash_message_error')}}</strong>
                    </div>
                    @endif
                    @if (session('flash_message_success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{session('flash_message_success')}}</strong>
                        </div>
                    @endif
                <div class="features_items"><!--features_items-->
                    @foreach ($transaksi as $order)
                    <h2 class="title text-center">List Transaksi {{$order->nama}}</h2>
                        <div class="col-sm-3">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <img src="{{asset('images/backend_images/kebaya/medium/'.$order->pemande->gambar)}}" alt="" />
                                        <h2>{{$order->nama}}</h2>
                                        <p>{{$order->alamat}}</p>
                                        {{-- <a href="{{url('/proses-transaksi/'.$order->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Proses Pembayaran</a> --}}
                                    </div>
                                    <div class="product-overlay">
                                        <div class="overlay-content">
                                            <h2>{{$order->nama}}</h2>
                                            <p>Pemesanan Barang</p>
                                            <p>{{$order->harga}}</p>
                                            <p>{{$order->alamat}}</p>
                                            <p>{{$order->no_hp}}</p>
                                            @if ($order->status_pembayaran == 0)
                                                <p>Belum Lunas</p>
                                            @endif
                                            <a href="{{url('/batal/'.$order->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-warning"></i>Batalkan Pesanan</a>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="choose">
                                            <ul class="nav nav-pills nav-justified">
                                                <li><a href="{{url('/proses-transaksi/'.$order->id)}}"><i class="fa fa-shopping-cart"></i>Bayar Transfer</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-6">
                                        <div class="choose">
                                            <ul class="nav nav-pills nav-justified">
                                                <li><a href="{{url('/credit-card/'.$order->id)}}"><i class="fa fa-shopping-cart"></i>Pembayaran Virtual Account</a></li>
                                            </ul>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    @endforeach

                    @foreach ($transaksiKebaya as $order)
                    @php
                        $galeri = App\GaleriPenjahit::where('id', $order->id_galeri)->first();
                    @endphp
                    @if (!empty($transaksi))
                        
                    @else
                        <h2 class="title text-center">List Transaksi {{$order->nama_pembeli}}</h2>
                    @endif
                        <div class="col-sm-3">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <img src="{{asset('images/backend_images/kebaya/medium/'.$order->galeri->gambar1)}}" alt="" />
                                        <h2>{{$order->nama_pembeli}}</h2>
                                        <p>{{$order->alamat}}</p>
                                        {{-- <a href="{{url('/proses-transaksi-kebaya/'.$order->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Proses Pembayaran</a> --}}
                                    </div>
                                    <div class="product-overlay">
                                        <div class="overlay-content">
                                            <h2>{{$order->nama_pembeli}}</h2>
                                            <p>Pembelian Barang</p>
                                            <p>{{$order->harga}}</p>
                                            <p>{{$order->alamat}}</p>
                                            <p>{{$order->no_hp}}</p>
                                            @if ($order->status_pembayaran == 0)
                                                <p>Belum Lunas</p>
                                            @endif
                                            <a href="{{url('/batal-beli/'.$order->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-warning"></i>Batalkan Pembelian</a>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="choose">
                                            <ul class="nav nav-pills nav-justified">
                                                <li><a href="{{url('/proses-transaksi-barang/'.$order->id)}}"><i class="fa fa-shopping-cart"></i>Bayar Transfer</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-6">
                                        <div class="choose">
                                            <ul class="nav nav-pills nav-justified">
                                                <li><a href="{{url('/credit-card-kebaya/'.$order->id)}}"><i class="fa fa-shopping-cart"></i>Pembayaran Virtual Account</a></li>
                                            </ul>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                </div><!--features_items-->
               
            </div>
        </div>
    </div>
@endsection