@extends('layouts.frontLayout.front_design')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    @if (session('flash_message_error'))
                    <div class="alert alert-error alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{session('flash_message_error')}}</strong>
                    </div>
                    @endif
                    @if (session('flash_message_success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{session('flash_message_success')}}</strong>
                        </div>
                    @endif
                    <h2>Detail Order atas Nama {{$order->nama_pembeli}}</h2>
                    <form enctype="multipart/form-data" method="post" action="{{url('/proses-transaksi-barang/'.$order->id)}}" novalidate="novalidate" name="add_order" id="add_order">
                        {{ csrf_field() }}

                        <input type="hidden" name="idOrder" id="idOrder" value="{{$order->id}}" />

                        @php
                            $galeri = App\GaleriPenjahit::where('id', $order->id_galeri)->first();
                            $penjahit = App\Penjahit::where('id', $galeri->id_penjahit)->first();
                        @endphp

                        <div class="col-md-6">
                            <h5>Nama Pembeli</h5> <input type="text" name="namaPemesan" id="namaPemesan" value="{{$order->nama_pembeli}}" disabled/>
                            <h5>Nama Pemilik Barang</h5> <input type="text" name="namaPemesan" id="namaPemesan" value="{{$order->pemande->nama}}" disabled/>
                            <h5>Alamat Pembeli</h5> <input type="text" name="namaPemesan" id="namaPemesan" value="{{$order->alamat}}" disabled/>
                            <h5>No. Hp</h5> <input type="text" name="namaPemesan" id="namaPemesan" value="{{$order->no_hp}}" disabled/>
                            <h5>Harga</h5> <input type="text" name="namaPemesan" id="namaPemesan" value="{{$order->harga}}" disabled/>
                            <h5>Tanggal Pemesanan</h5> <input type="text" name="namaPemesan" id="namaPemesan" value="{{$order->created_at}}" disabled/>
                        </div>

                        <div class="col-md-12">
                            <hr><hr>
                        </div>

                        <div class="col-md-8">
                            <h2>Form Konfirmasi Pembayaran</h2>
                            <h5>Nomor Rekening Tujuan</h5> <input disabled value="53428920">
                            <h5>Jenis Bank</h5> <input disabled value="BCA">
                            <h5>Atas Nama</h5> <input disabled value="Pande Bawa">
                            <h5>Total yang Harus Dibayar</h5> <input disabled name="total" id="total" value="Rp. {{$order->harga}}"/>
                            <input type="hidden" name="harga" id="harga" value="{{$galeri->harga}}">
                            <input type="hidden" name="jumlah" id="jumlah" value="{{$galeri->harga}}">
                            <h5>Bukti Transfer</h5> <input type="file" name="buktiTransfer" id="buktiTransfer"/>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-default">Bayar</button>
                        </div>
                        
                    </form>
                </div><!--/login form-->
            </div>
        </div>
    </div>
    <br><br>
@endsection