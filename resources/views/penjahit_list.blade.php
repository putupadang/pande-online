@extends('layouts.frontLayout.front_design')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Daftar Pemande</h2>

                    @foreach ($penjahits as $penjahit)
                    @if ($penjahit->status == 1)
                    <div class="col-sm-3">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="{{asset('images/backend_images/kebaya/medium/'.$penjahit->gambar)}}" alt="" />
                                    <h2>{{$penjahit->nama}}</h2>
                                    <p>{{$penjahit->deskripsi}}</p>
                                    <a href="{{url('/order/'.$penjahit->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Pesan</a>
                                </div>
                                <div class="product-overlay">
                                    <div class="overlay-content">
                                        <h2>{{$penjahit->nama}}</h2>
                                        {{-- <p>Mulai Jahit : {{$penjahit->mulaiJahit}}</p>
                                        <p>Pengalaman Jahit : {{$penjahit->pengalaman}}</p> --}}
                                        <a href="{{url('/order/'.$penjahit->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Pesan</a>
                                    </div>
                                </div>
                            </div>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="{{url('/pemande-detail/'.$penjahit->id)}}"><i class="fa fa-plus-square"></i>Lihat Detail</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    
                </div><!--features_items-->
                <ul class="pagination">
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">&raquo;</a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection