@extends('layouts.frontLayout.front_design')

@section('content')

<section id="slider"><!--slider-->
    <div class="container" style="height: 500px;">
        <div class="row">
            <div class="col-sm-12" style="height: 100%">
                <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                    
                    <ol class="carousel-indicators">
                        <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#slider-carousel" data-slide-to="1"></li>
                        <li data-target="#slider-carousel" data-slide-to="2"></li>
                        <li data-target="#slider-carousel" data-slide-to="3"></li>
                    </ol>
                    
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-sm-6" style="height: 593px">
                                <h1><span>Pande</span>-Online</h1>
                                <h3>Welcome to Pande Online</h3>
                                <p style="text-align: justify">Tempat dimana anda bisa mendapatkan jasa seorang pande besi, dari membeli produk-produk hasil karya para pemande, serta membuat barang atau alat dari besi sesuai dengan keinginan.</p>
                                {{-- <button type="button" class="btn btn-default get">Get it now</button> --}}
                            </div>
                            <div class="col-sm-6">
                                <img src="{{asset('images/frontend_images/pande1.jpeg')}}" style="width: 300px; height: 500px; float: right" class="pricing" alt="" />
                            </div>
                        </div>

                        <div class="item">
                            <div class="col-sm-6" style="height: 593px">
                                <h1><span>Pande</span>-Online</h1>
                                <h3>Prapen Pande</h3>
                                <p style="text-align: justify">Prapen merupakan sebuah simbol atau identitas dari orang pande. Tempat seorang pande melakukan pekerjaannya memande adalah prapen. Prapen dikatakan sebagai simbol dari kepandean karena orang pande memiliki keahlian khusus sebagai pengolah besi atau yang sering disebut dengan memande, jadi secara tidak langsung bangunan prapen tersebut terdapat di setiap rumah atau pekarangan warga pande,selain itu prapen juga merupakan tempat untuk memuja dewa yaitu dewa brahma.</p>
                                {{-- <button type="button" class="btn btn-default get">Get it now</button> --}}
                            </div>
                            <div class="col-sm-6">
                                <img src="{{asset('images/frontend_images/pande2.jpeg')}}" style="width: 300px; height: 500px; float: right" class="pricing" alt="" />
                            </div>
                        </div>
                        
                        <div class="item">
                            <div class="col-sm-6" style="height: 593px">
                                <h1><span>Pande</span>-Online</h1>
                                <h3>Warga Pande</h3>
                                <p style="text-align: justify">Dalam Lontar Lalintahan Warga Pande di Bali yang terdapat pada “Prasasti Pande Kaba – Kaba”, koleksi Gedong Krtya nomor 2864 menjelaskan tentang kisah asal usul warga pande di Bali. Mpu Brahma Wisesa adalah keturunan dari Mpu Rajakrta yang menurunkan semua kelompok Pande di Bali. Lalintahan warga pande di Bali yang dirangkum oleh Kembar (2014 : 6), menceritakan anak Mpu Gandring Sakti dan Dyah Amertatma yaitu Mpu Brahmana Dwala yang juga merupakan keturunan dari Mpu Brahma Raja yang berasal dari madura yang mabhiseka nama beliau yaitu Mpu Pandhya Bhumi Sakti, Mpu Dwala melakukan yoga semadi di indrakila dengan tujuan untuk memohon kepada dewata agar arwah kakek beliau yaitu Mpu Pandhya Bumi Sakti turun ke marcepada (dunia), agar memberikan penjelasan tentang asal usulnya.</p>
                                {{-- <button type="button" class="btn btn-default get">Get it now</button> --}}
                            </div>
                            
                            <div class="col-sm-6">
                                <img src="{{asset('images/frontend_images/pande3.jpeg')}}" style="width: 300px; height: 500px; float: right" class="pricing" alt="" />
                            </div>
                        </div>
                        
                        <div class="item">
                            <div class="col-sm-6" style="height: 593px">
                                <h1><span>Pande</span>-Online</h1>
                                <h3>Pura Penataran Pande Besakih</h3>
                                <p style="text-align: justify">Pura Penataran Pande Besakih tercatat pada Bhisama pertama Warga Pande dimana berisi agar Warga Pande tidak lupa menyungsung pura Besakih dan Pura Penataran Pande di Besakih. Pura Penataran Pande Besakih terletak di Besakih, Kabupaten Karangasem, sebelah kiri dibelakang Pura Besakih, masuk dari depan Pura, dengan wastra berwarna merah.</p>
                                {{-- <button type="button" class="btn btn-default get">Get it now</button> --}}
                            </div>
                            <div class="col-sm-6">
                                <img src="{{asset('images/frontend_images/pande4.jpeg')}}" style="width: 300px; height: 500px; float: right" class="pricing" alt="" />
                            </div>
                        </div>
                    </div>
                    
                    <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
                
            </div>
        </div>
    </div>
</section><!--/slider-->



@endsection