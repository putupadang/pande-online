@extends('layouts.frontLayout.front_design')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">List Galeri Pemande</h2>

                    @foreach ($galeris as $galeri)
                    @php
                        $id_pemande = $galeri->id_pemande;
                        $penjahit = App\Penjahit::where(['id' => $id_pemande])->first();
                    @endphp
                    @if ((!empty($galeri)))
                    <div class="col-sm-3">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="{{asset('images/backend_images/kebaya/medium/'.$galeri->gambar1)}}" alt="" />
                                    <h2>{{$galeri->nama_baju}}</h2>
                                    <p>{{$galeri->deskripsi}}</p>
                                    <a href="{{url('/barang-detail/'.$galeri->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Beli</a>
                                </div>
                                <div class="product-overlay">
                                    <div class="overlay-content">
                                        <h2>{{$galeri->nama_baju}}</h2>
                                        <p>Dibuat Oleh : {{$penjahit->nama}}</p>
                                        <p>Stok Barang : {{$galeri->stok}}</p>
                                        <p>{{$galeri->deskripsi}}</p>
                                        <a href="{{url('/barang-detail/'.$galeri->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Beli</a>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="{{url('/kebaya-detail/'.$galeri->id)}}"><i class="fa fa-plus-square"></i>Lihat Detail galeri</a></li>
                                </ul>
                            </div> --}}
                        </div>
                    </div>
                    @else
                        <div class="col-sm-12" style="text-align:center">
                            <h2>Galeri Kebaya Kosong !</h2>
                        </div>
                    @endif
                    @endforeach
                    
                </div><!--features_items-->
                <ul class="pagination">
                    <li class="active"><a href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">&raquo;</a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection