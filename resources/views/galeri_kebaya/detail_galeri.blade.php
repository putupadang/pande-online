@extends('layouts.frontLayout.front_design')

@section('content')
    <div class="container">
        <div class="row">
            @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{($error)}}</strong>    
                </div>
            @endforeach
            @endif
            @if (session('flash_message_error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{session('flash_message_error')}}</strong>
            </div>
            @endif
            @if (session('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{session('flash_message_success')}} Silakan untuk Melanjutkan ke Proses Pembayaran!</strong>
                    {{-- <a href="{{url('/view-transaksi')}}"><button type="submit" class="btn btn-default">Lanjut Proses Pembayaran</button></a> --}}
                </div>
            @endif                        
            <div class="col-sm-12 padding-center">
                <div class="product-details"><!--product-details-->
                    <div class="col-sm-6">
                        <div class="view-product">
                            <img src="{{asset('images/backend_images/kebaya/medium/'.$galeri->gambar1)}}" alt="" />
                            <img src="{{asset('images/backend_images/kebaya/medium/'.$galeri->gambar2)}}" alt="" />
                            <img src="{{asset('images/backend_images/kebaya/medium/'.$galeri->gambar3)}}" alt="" />
                            <img src="{{asset('images/backend_images/kebaya/medium/'.$galeri->gambar4)}}" alt="" />
                            <img src="{{asset('images/backend_images/kebaya/medium/'.$galeri->gambar5)}}" alt="" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        @php
                            $penjahit = App\Penjahit::where('id', $galeri->id_pemande)->first();
                        @endphp
                        <div class="product-information"><!--/product-information-->
                            <img src="images/product-details/new.jpg" class="newarrival" alt="" />
                            <span>
                                <span>{{$galeri->nama_baju}}</span>
                            </span>
                            <h2>Rp.{{$galeri->harga}}</h2>
                            <p>Dibuat Tanggal : {{date('d-m-Y', strtotime($galeri->created_at))}}</p>
                            <p>Dibuat Oleh : {{$penjahit->nama}}</p>
                            <p>Stok Barang : {{$galeri->stok}}</p>
                            <p>{{$galeri->deskripsi}}</p>
                        </div><!--/product-information-->
                    </div>
                </div><!--/product-details-->
                
                <div class="category-tab shop-details-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#reviews" data-toggle="tab">Form Pembelian Barang</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">   
                        <div class="tab-pane fade active in" id="reviews" >
                            <div class="col-sm-7">
                                <p><b>Isi Form dibawah !</b></p>
                                <form action="{{url('/barang-detail/'.$galeri->id)}}" method="POST">
                                    {{ csrf_field() }}

                                    @if (!empty(Auth::user()))
                                        @php
                                            $id_pelanggan = Auth::user()->id;
                                            $pelanggan = Auth::user();
                                        @endphp
                                        <input type="hidden" name="namaPembeli" id="namaPembeli" value="{{$pelanggan->name}}" />
                                        <input type="hidden" name="emailPembeli" id="emailPembeli" value="{{$pelanggan->email}}" />
                                        <input type="hidden" name="noHP" id="noHP" value="{{$pelanggan->no_hp}}" />
                                        <input type="hidden" name="alamat" id="alamat" value="{{$pelanggan->alamat}}" />
                                    @else
                                        @php
                                            $id_pelanggan = null;
                                        @endphp
                                        <input class="form-control"  type="text" placeholder="Nama Anda" name="namaPembeli"/><br>
                                        <input class="form-control" type="email" placeholder="Email Anda" name="emailPembeli"/><br>
                                        <input class="form-control" type="text" placeholder="Nomor HP" name="noHP"/><br>
                                        <textarea name="alamat" placeholder="Alamat" class="form-control"></textarea><br>
                                    @endif

                                    <input type="hidden" name="idPelanggan" id="idPelanggan" value="{{$id_pelanggan}}" />
                                    <input type="hidden" name="idGaleri" id="idGaleri" value="{{$galeri->id}}" />
                                    <input type="hidden" name="idPenjahit" id="idPenjahit" value="{{$penjahit->id}}" />

                                    <input type="text" class="form-control" placeholder="Jumlah Barang yang dibeli" name="jml_stok"><br>
                                    
                                    <button type="submit" class="btn btn-default pull-left">
                                        Beli
                                    </button>
                                    {{-- <input type="submit" class="btn btn-default pull-left" value="Beli Kebaya"> --}}
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div><!--/category-tab-->
                
            </div>
        </div>
    </div>
@endsection