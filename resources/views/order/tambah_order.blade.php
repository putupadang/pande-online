@extends('layouts.frontLayout.front_design')

@section('content')
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-1">
                    <div class="login-form"><!--login form-->
                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-error alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{($error)}}</strong>    
                                </div>
                            @endforeach
                        @endif
                        @if (session('flash_message_error'))
                        <div class="alert alert-error alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{session('flash_message_error')}}</strong>
                        </div>
                        @endif
                        @if (session('flash_message_success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{session('flash_message_success')}} Silakan untuk Melanjutkan ke Proses Pembayaran!</strong>
                                {{-- <a href="{{url('/view-transaksi')}}"><button type="submit" class="btn btn-default">Lanjut Proses Pembayaran</button></a> --}}
                            </div>
                        @endif
                        <h2>Form Order</h2>
                        <form enctype="multipart/form-data" method="post" action="{{url('/order/{id}')}}" novalidate="novalidate" name="add_order" id="add_order">
                            {{ csrf_field() }}

                            @if (!empty(Auth::user()))
                                @php
                                    $id_pelanggan = Auth::user()->id;
                                    $pelanggan = Auth::user();
                                @endphp
                                <input type="hidden" name="namaPemesan" id="namaPemesan" value="{{$pelanggan->name}}" />
                                <input type="hidden" name="emailPemesan" id="emailPemesan" value="{{$pelanggan->email}}" />
                                <input type="hidden" name="noHP" id="noHP" value="{{$pelanggan->no_hp}}" />
                                <input type="hidden" name="alamat" id="alamat" value="{{$pelanggan->alamat}}" />
                            @else
                                @php
                                    $id_pelanggan = null;
                                @endphp
                                <h5>Nama Pemesan</h5> <input type="text" name="namaPemesan" id="namaPemesan" placeholder="Nama"/>
                                <h5>Email Pemesan</h5> <input type="text" name="emailPemesan" id="emailPemesan" placeholder="Email"/>
                                <h5>No HP</h5> <input type="number" name="noHP" id="noHP" placeholder="+62"/>
                                <h5>Alamat</h5> <textarea type="text" name="alamat" id="alamat" placeholder="Alamat"></textarea>
                            @endif

                            <input type="hidden" name="idPelanggan" id="idPelanggan" value="{{$id_pelanggan}}" />
                            <input type="hidden" name="idPenjahit" id="idPenjahit" value="{{$penjahit->id}}" />
                            <input type="hidden" name="namaPenjahit" id="namaPenjahit" value="{{$penjahit->nama}}" />
                            <input type="hidden" name="hargaJahit" id="hargaJahit" value="{{$penjahit->harga_jasa}}" />
                            {{-- <h5>Nama Pemesan</h5> <input type="text" name="namaPemesan" id="namaPemesan" placeholder="Nama"/>
                            <h5>Email Pemesan</h5> <input type="text" name="emailPemesan" id="emailPemesan" placeholder="Email"/> --}}
                            <h5>Deskripsi Pesanan</h5> <textarea name="deskripsi_pesanan" id="deskripsi_pesanan" placeholder="Deskripsi Pesanan"></textarea>
                            <h5>Contoh Gambar 1</h5> <input type="file" name="gambar" id="gambar" placeholder="Masukkan Contoh Gambar">
                            <h5>Contoh Gambar 2</h5> <input type="file" name="gambar1" id="gambar1" placeholder="Masukkan Contoh Gambar">
                            <h5>Contoh Gambar 3</h5> <input type="file" name="gambar2" id="gambar2" placeholder="Masukkan Contoh Gambar">
                            <h5>Contoh Gambar 4</h5> <input type="file" name="gambar3" id="gambar3" placeholder="Masukkan Contoh Gambar">
                            <h5>Contoh Gambar 5</h5> <input type="file" name="gambar4" id="gambar4" placeholder="Masukkan Contoh Gambar">

                            {{-- <h5>Pilihan Kain</h5>
                            <span>
                                <label>Menggunakan Kain Pilihan Pribadi<input type="radio" value="0" name="pilihanKain" id="pilihanKain"/></label>
                                <label style="margin-left:20px">Menggunakan Kain Pilihan Penjahit<input type="radio" value="1" name="pilihanKain" id="pilihanKain"/></label><br>
                            </span> --}}

                            <button type="submit" class="btn btn-default">Order</button>
                        </form>
                    </div><!--/login form-->
                </div>
            </div>
        </div>
        <br><br>
@endsection