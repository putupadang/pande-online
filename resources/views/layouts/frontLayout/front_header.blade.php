<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> +62 81 999 111 222</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> aa@stikom-bali.ac.id</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->
    
    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="index.html"><img src="images/home/logo.png" alt="" /></a>
                    </div>
                    <div class="btn-group pull-right">
                        <div class="btn-group">
                            {{-- <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                USA
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Canada</a></li>
                                <li><a href="#">UK</a></li>
                            </ul> --}}
                        </div>
                        
                        <div class="btn-group">
                            {{-- <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                DOLLAR
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Canadian Dollar</a></li>
                                <li><a href="#">Pound</a></li>
                            </ul> --}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            @if (!empty(Auth::user()))
                                @php
                                    $nama = Auth::user()->name;
                                @endphp
                                <li><a href="/home"><i class="fa fa-user"></i>Home</a></li>
                                <li><a href="{{url('/view-transaksi-login')}}"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                            @else
                                <li><a href="{{url('/view-transaksi')}}"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                                <li><a href="{{url('/home')}}"><i class="fa fa-lock"></i> Login</a></li>
                            @endif
                            {{-- <li><a href="/home"><i class="fa fa-user"></i>Halo, {{$nama}}</a></li> --}}
                            {{-- <li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li> --}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="/" class="active">Home</a></li>
                            <li class="dropdown"><a href="{{url('/')}}">Pande Online<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="{{url('/registrasi-pemande')}}">Registrasi Pemande</a></li>
                                    <li><a href="{{url('/pemande/home')}}">Login Pemande</a></li> 
                                    <li><a href="{{url('/list-barang')}}">Beli</a></li> 
                                    @if (!empty(Auth::user()))
                                        <li><a href="{{url('/view-transaksi-login')}}">Cart</a></li>
                                    @else
                                        <li><a href="{{url('/view-transaksi')}}">Cart</a></li>
                                    @endif
                                </ul>
                            </li>
                            <li><a href="{{url('/list-pemande')}}">List Pemande</a></li>
                            <li><a href="{{url('/list-barang')}}">List Produk</a></li>

                            {{-- <li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="blog.html">Blog List</a></li>
                                    <li><a href="blog-single.html">Blog Single</a></li>
                                </ul>
                            </li>  --}}

                            {{-- <li><a href="404.html">404</a></li> --}}

                            {{-- <li><a href="contact-us.html">Kontak Kami</a></li> --}}
                        </ul>
                    </div>
                </div>
                {{-- <div class="col-sm-3">
                    <div class="search_box pull-right">
                        <input type="text" placeholder="Search"/>
                    </div>
                </div> --}}
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->