<!--Header-part-->
<div id="header">
    <h3><a href="dashboard.html" style="margin:13px">Pemande Page</a></h3>
</div>
<!--close-Header-part--> 
<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
<ul class="nav">
    <li class=""><a title="" href="{{url('pemande/setting/'.Auth::user()->id)}}"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>
    <li class="">
        <a title="" href="{{url('logout')}} "><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a>

        {{-- <a class="dropdown-item" href="{{ url('logout-penjahit') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="icon icon-share-alt"></i> <span class="text">{{ __('Logout') }}</span>
        </a>

        <form id="logout-form" action="{{ url('logout-penjahit') }}" method="POST" style="display: none;">
            @csrf
        </form> --}}
    </li>
</ul>
</div>
<!--close-top-Header-menu-->

<!--start-top-serch-->
<div id="search">
    <input type="text" placeholder="Search here..."/>
    <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>
<!--close-top-serch-->