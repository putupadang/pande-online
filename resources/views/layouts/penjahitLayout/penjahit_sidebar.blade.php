<!--sidebar-menu-->
<div id="sidebar">
  <a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
    <ul>
      <li class="active"><a href="{{url('pemande/home')}}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>

      <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Order</span></a>
        <ul>
          <li><a href="{{url('/pemande/view-order')}}">Order Pesanan</a></li>
          <li><a href="{{url('/pemande/view-order-pande')}}">Order Pembelian</a></li>
        </ul>
      </li>

      <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Galeri</span></a>
        <ul>
          <li><a href="{{url('/pemande/tambah-galeri')}}">Tambah Galeri</a></li>
          <li><a href="{{url('/pemande/lihat-galeri')}}">Lihat Galeri</a></li>
        </ul>
      </li>
    </ul>
</div>
<!--sidebar-menu-->