<!--sidebar-menu-->
<div id="sidebar">
  <a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
    <ul>
      <li class="active"><a href="{{url('admin/dashboard')}}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
      
      <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Pemande</span></a>
        <ul>
          {{-- <li><a href="{{url('admin/add-penjahit')}}">Tambah Penjahit</a></li> --}}
          <li><a href="{{url('admin/view-pemande')}}">Lihat List Pemande</a></li>
        </ul>
      </li>

      <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Order</span></a>
        <ul>
          <li><a href="{{url('/admin/order-admin')}}">List Order Pesanan</a></li>
          <li><a href="{{url('/admin/order')}}">List Order Pembelian</a></li>
        </ul>
      </li>

      <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Transaksi</span></a>
        <ul>
          <li><a href="{{url('/admin/view-transaksi')}}">List Transaksi</a></li>
          {{-- <li><a href="{{url('#')}}">List Transaksi Baju Kebaya</a></li> --}}
        </ul>
      </li>

      {{-- <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Model Kebaya</span></a>
        <ul>
          <li><a href="{{url('/admin/tambah-kebaya')}}">Tambah Model Kebaya</a></li>
          <li><a href="{{url('/admin/lihat-kebaya')}}">Lihat List Model Kebaya</a></li>
        </ul>
      </li> --}}
    </ul>
</div>
<!--sidebar-menu-->