@extends('layouts.adminLayout.admin_design')

@section('content')

<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
      <div id="content-header">
        <div id="breadcrumb"> <a hre ="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
      </div>
    <!--End-breadcrumbs-->
    
    <!--Action boxes-->
      <div class="container-fluid">
        <div class="quick-actions_homepage">
          <ul class="quick-actions">
            <li class="bg_lb"> <a href="{{url('admin/dashboard')}}""> <i class="icon-dashboard"></i> <span class="label label-important"></span> My Dashboard </a> </li>
            
            {{-- <li class="bg_lg span3"> <a href="charts.html"> <i class="icon-signal"></i> Charts</a> </li>
            <li class="bg_ly"> <a href="widgets.html"> <i class="icon-inbox"></i><span class="label label-success">101</span> Widgets </a> </li> --}}
            
            <li class="bg_lo"> <a href="{{url('admin/view-pemande')}}"> <i class="icon-th"></i>Pemande</a> </li>
            <li class="bg_ly"> <a href="{{url('admin/order-admin')}}""> <i class="icon-th"></i>Order Pesanan</a> </li>
            <li class="bg_ls"> <a href="{{url('admin/order')}}""> <i class="icon-th"></i>Order Pembelian</a> </li>
            <li class="bg_lr"> <a href="{{url('admin/view-transaksi')}}""> <i class="icon-th"></i>Transaksi</a> </li>            
            
            {{-- <li class="bg_ls"> <a href="grid.html"> <i class="icon-fullscreen"></i> Full width</a> </li> --}}
            
            {{-- <li class="bg_lo span3"> <a href="form-common.html"> <i class="icon-th-list"></i> Forms</a> </li> --}}
            
            {{-- <li class="bg_ls"> <a href="buttons.html"> <i class="icon-tint"></i> Buttons</a> </li>
            <li class="bg_lb"> <a href="interface.html"> <i class="icon-pencil"></i>Elements</a> </li>
            <li class="bg_lg"> <a href="calendar.html"> <i class="icon-calendar"></i> Calendar</a> </li>
            <li class="bg_lr"> <a href="error404.html"> <i class="icon-info-sign"></i> Error</a> </li> --}}
          </ul>
        </div>
    <!--End-Action boxes-->    
    
<!--Chart-box-->    
    
<!--End-Chart-box--> 
    <hr/>
    <div class="row-fluid">
        
        </div>
        </div>
    </div>
    </div>
</div>
<!--end-main-container-part-->

@endsection