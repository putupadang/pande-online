@extends('layouts.adminLayout.admin_design')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Order</a><a href="#" class="current">Lihat List Order Pemesanan</a> </div>
        <h1>List Order Pemesanan</h1>
        @if (session('flash_message_error'))
            <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{session('flash_message_error')}}</strong>
            </div>
        @endif
        @if (session('flash_message_success'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{session('flash_message_success')}}</strong>
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                    <h5>List Order Pemesanan</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                        <th>Nama Pemande</th>
                        <th>Nama Pemesan</th>
                        <th>Alamat Pemesan</th>
                        <th>Deskripsi Pesanan</th>
                        <th>Lama Pengerjaan</th>
                        <th>Status Pembayaran</th>
                        <th>Status Order</th>
                        <th>Pilihan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $Order)

                        {{-- @php
                            $nama = $Order->id_pemande;
                            $detail = App\Penjahit::where(['id' => $nama])->first();
                            dd($detail);
                        @endphp --}}
                        
                            <tr class="gradeX">
                                {{-- <td>{{$Order->id}}</td> --}}
                                <td>{{$Order->pemande->nama}}</td>
                                <td>{{$Order->nama}}</td>
                                <td>{{$Order->alamat}}</td>
                                <td>{{$Order->deskripsi_pesanan}}</td>
                                <td>{{$Order->lama_hari}} Hari</td>

                                @if ($Order->status_bayar == 0)
                                    <td>Belum Melakukan Pembayaran</td>
                                @endif
                                @if ($Order->status_bayar == 1)
                                    <td>Sudah Melakukan Pembayaran</td>
                                @endif
                                @if ($Order->status_bayar == 2)
                                    <td>Pembayaran Dikembalikan</td>
                                @endif
                                @if ($Order->status_bayar == 3)
                                    <td>Sudah Melakukan Pembayaran</td>
                                @endif
                                @if ($Order->status_bayar == 4)
                                    <td>Pesanan Dibatalkan</td>
                                @endif

                                @if ($Order->status_order == 0)
                                    <td>Menunggu Konfirmasi</td>
                                @endif
                                @if ($Order->status_order == 1)
                                    <td>Pesanan Sedang Proses</td>
                                @endif
                                @if ($Order->status_order == 2)
                                    <td>Pesanan Sudah Selesai</td>
                                @endif
                                @if ($Order->status_order == 3)
                                    <td>Pesanan Ditolak Pemande</td>
                                @endif
                                @if ($Order->status_order == 4)
                                    <td>Pesanan Dibatalkan</td>
                                @endif
                                @if ($Order->status_order == 5)
                                    <td>Uang di kirim ke Pemande</td>
                                @endif

                                <td class="center">
                                    @if ($Order->status_bayar == 3 && $Order->status_order == 2)
                                        <a href="{{url('/pemande-detail/'.$Order->id_pemande)}}"  class="btn btn-info btn-mini">Detail Pemande</a> | 
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Order</a> |
                                        {{-- <a href=" {{url('/admin/refund-order/'.$Order->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Refund</a> | --}}
                                        <a href=" {{url('/admin/setor-order-pesanan/'.$Order->id)}} " class="btn btn-warning btn-mini">Setor Pemande</a>
                                    @endif
                                    @if ($Order->status_bayar == 3 && $Order->status_order == 1)
                                        <a href="{{url('/pemande-detail/'.$Order->id_pemande)}}"  class="btn btn-info btn-mini">Detail Pemande</a> | 
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Order</a>
                                        {{-- <a href=" {{url('/admin/refund-order/'.$Order->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Refund</a> --}}
                                    @endif
                                    @if ($Order->status_bayar == 3 && $Order->status_order == 3)
                                        <a href="{{url('/pemande-detail/'.$Order->id_pemande)}}"  class="btn btn-info btn-mini">Detail Pemande</a> | 
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Order</a>
                                        {{-- <a href=" {{url('/admin/refund-order/'.$Order->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Refund</a> --}}
                                    @endif
                                    @if($Order->status_bayar == 3 && $Order->status_order == 0)
                                        <a href="{{url('/pemande-detail/'.$Order->id_pemande)}}"  class="btn btn-info btn-mini">Detail Pemande</a> | 
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Order</a>
                                        {{-- <a href=" {{url('/admin/refund-order/'.$Order->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Refund</a> --}}
                                    @endif
                                    @if ($Order->status_bayar == 2)
                                        <a href="{{url('/pemande-detail/'.$Order->id_pemande)}}"  class="btn btn-info btn-mini">Detail Pemande</a> |
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Order</a>                                        
                                    @endif
                                    @if ($Order->status_bayar == 1 && $Order->status_order == 1)
                                        <a href="{{url('/pemande-detail/'.$Order->id_pemande)}}"  class="btn btn-info btn-mini">Detail Pemande</a> |
                                        <a href="{{url('/admin/verifikasi-order/'.$Order->id)}}"  class="btn btn-primary btn-mini">Lunas</a> |
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Order</a>                                        
                                    @endif
                                    @if ($Order->status_bayar == 0 && $Order->status_order == 0)
                                        <a href="{{url('/pemande-detail/'.$Order->id_pemande)}}"  class="btn btn-info btn-mini">Detail Pemande</a> |
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Order</a>                                        
                                    @endif
                                    @if ($Order->status_bayar == 4)
                                        <a href="{{url('/pemande-detail/'.$Order->id_pemande)}}"  class="btn btn-info btn-mini">Detail Pemande</a> |
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Order</a>                                        
                                    @endif
                                    @if ($Order->status_bayar == 3 && $Order->status_order == 5)
                                        <a href="{{url('/pemande-detail/'.$Order->id_pemande)}}"  class="btn btn-info btn-mini">Detail Pemande</a> |
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Order</a>                                        
                                    @endif
                                    @if ($Order->status_bayar == 0 && $Order->status_order == 1)
                                        <a href="{{url('/pemande-detail/'.$Order->id_pemande)}}"  class="btn btn-info btn-mini">Detail Pemande</a> |
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Order</a>                                        
                                    @endif
                                </td>
                            </tr>
                            
                            <div id="myModal{{$Order->id}}" class="modal hide">

                                <div class="modal-header">
                                <button data-dismiss="modal" class="close" type="button">×</button>
                                <h3>Detail Order Pelanggan {{$Order->nama_pemesan}}</h3>
                                </div>
                                
                                <div class="modal-body">
                                    <p>ID Order : {{$Order->id}}</p>
                                    <p>Nama Pemesan : {{$Order->nama}}</p>
                                    <p>Nama Pemande yang dipesan : {{$Order->pemande->nama}}</p>
                                    <p>Harga Total : {{$Order->harga_total}}</p>
                                    <p>No HP Pemesan : {{$Order->no_hp}}</p>
                                    <p>Alamat Pemesan : {{$Order->alamat}}</p>
                                    <p>Tanggal Order : {{$Order->created_at}}</p>
                                    @if ($Order->gambar_pesanan != null)
                                        <p>Contoh Gambar : </p> <img src="{{asset('images/frontend_images/orders/small/'.$Order->gambar_pesanan)}}" alt="">
                                    @endif
                                    <hr>
                                    @if ($Order->status_bayar == 0)
                                        <p>Status Pembayaran : Belum Melakukan Pembayaran</p>
                                    @endif
                                    @if ($Order->status_bayar == 1)
                                        <p>Status Pembayaran : Sudah Melakukan Pembayaran</p>
                                    @endif
                                    @if ($Order->status_bayar == 2)
                                        <p>Status Pembayaran : Pembayaran Dikembalikan</p>
                                    @endif
                                    @if ($Order->status_bayar == 3)
                                        <p>Status Pembayaran : Sudah Melakukan Pembayaran</p>
                                    @endif
                                    @if ($Order->status_bayar == 4)
                                        <p>Status Pembayaran : Pesanan Dibatalkan</p>
                                    @endif
                                    @if ($Order->status_order == 5)
                                        <p>Status Setor : Uang di kirim ke Pemande</p>
                                    @endif
                                    @if ($Order->bukti_setor != null)
                                        <p>
                                            Bukti Setor Transfer : <br><img src="{{asset('images/backend_images/kebaya/medium/'.$Order->bukti_setor)}}" style="width:250px">
                                        </p>
                                    @endif
                                </div>
                            </div>
                            
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection