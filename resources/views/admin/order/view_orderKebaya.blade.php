@extends('layouts.adminLayout.admin_design')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Order Pembelian</a><a href="#" class="current">Lihat List Order Pembelian</a> </div>
        <h1>List Order Pembelian</h1>
        @if (session('flash_message_error'))
            <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{session('flash_message_error')}}</strong>
            </div>
        @endif
        @if (session('flash_message_success'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{session('flash_message_success')}}</strong>
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                    <h5>List Order Pembelian</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                        <th>Nama Pembeli</th>
                        <th>Email Pembeli</th>
                        <th>Alamat Pembeli</th>
                        <th>Status Pembayaran</th>
                        <th>Status Pembelian</th>
                        <th>Pilihan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $Order)
                            <tr class="gradeX">
                                <td>{{$Order->nama_pembeli}}</td>
                                <td>{{$Order->email_pembeli}}</td>
                                <td>{{$Order->alamat}}</td>

                                @if ($Order->status_bayar == 0)
                                    <td>Belum Melakukan Pembayaran</td>
                                @endif
                                @if ($Order->status_bayar == 1)
                                    <td>Sudah Melakukan Pembayaran</td>
                                @endif
                                @if ($Order->status_bayar == 3)
                                    <td>Pembayaran Dikembalikan</td>
                                @endif
                                @if ($Order->status_bayar == 2)
                                    <td>Sudah Melakukan Pembayaran</td>
                                @endif
                                @if ($Order->status_bayar == 4)
                                    <td>Pembelian Dibatalkan</td>
                                @endif
                                @if ($Order->status_bayar == 5)
                                    <td>Uang Telah Disetor ke Pemande</td>
                                @endif

                                {{-- @if ($Order->status_terjual == 4)
                                    <td>Pembelian Dibatalkan</td>
                                @endif --}}
                                @if ($Order->status_terjual == 0)
                                    <td>Menunggu Konfirmasi</td>
                                @endif
                                @if ($Order->status_terjual == 1)
                                    <td>Barang Telah Terjual</td>
                                @endif
                                @if ($Order->status_terjual == 2)
                                    <td>Pembelian Dibatalkan</td>
                                @endif
                                @if ($Order->status_terjual == 4)
                                    <td>Pembelian Dibatalkan</td>
                                @endif
                                <td class="center">
                                    @if($Order->status_bayar == 1)
                                        <a href=" {{url('admin/verifikasi-order-pembelian/'.$Order->id)}} " class="btn btn-primary btn-mini">Sudah Lunas</a> |
                                        {{-- <a href=" {{url('/admin/refund-order-pembelian/'.$Order->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Refund</a> |  --}}
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Pembelian</a>
                                    @endif
                                    @if ($Order->status_bayar == 2)
                                        {{-- <a href=" {{url('/admin/refund-order-pembelian/'.$Order->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Refund</a> |  --}}
                                        <a href=" {{url('/admin/setor-order-pembelian/'.$Order->id)}} " class="btn btn-warning btn-mini">Setor Pemande</a> |
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Pembelian</a>                                        
                                    @endif
                                    @if ($Order->status_bayar == 3)
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Pembelian</a>                                        
                                    @endif
                                    @if ($Order->status_bayar == 0)
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Pembelian</a>                                        
                                    @endif
                                    @if ($Order->status_bayar == 4)
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Pembelian</a>                                        
                                    @endif
                                    @if ($Order->status_bayar == 5)
                                        <a href="#myModal{{$Order->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Pembelian</a>                                        
                                    @endif
                                </td>
                            </tr>
                            
                            <div id="myModal{{$Order->id}}" class="modal hide">

                                <div class="modal-header">
                                <button data-dismiss="modal" class="close" type="button">×</button>
                                <h3>Detail Order Pelanggan {{$Order->nama_pembeli}}</h3>
                                </div>
                                
                                <div class="modal-body">
                                    <p>ID Order : {{$Order->id}}</p>
                                    <p>Nama Pembeli : {{$Order->nama_pembeli}}</p>
                                    <p>Pemilik Barang : {{$Order->galeri->pemande->nama}}</p>
                                    <p>Harga Barang : {{$Order->galeri->harga}}</p>
                                    <p>No HP Pembeli : {{$Order->no_hp}}</p>
                                    <p>Alamat Pembeli : {{$Order->alamat}}</p>
                                    <p>Tanggal Pembelian : {{date('d-m-Y', strtotime($Order->created_at))}}</p>
                                    <p>
                                        Barang yang dibeli : <br><img src="{{asset('images/backend_images/kebaya/medium/'.$Order->galeri->gambar1)}}" alt="" style="width:250px">
                                    </p>
                                    <hr>
                                    @if ($Order->status_bayar == 0)
                                    <p>Status Pembayaran : Belum Melakukan Pembayaran</p>
                                    @endif
                                    @if ($Order->status_bayar == 1)
                                        <p>Status Pembayaran : Sudah Melakukan Pembayaran</p>
                                    @endif
                                    @if ($Order->status_bayar == 3)
                                        <p>Status Pembayaran : Pembayaran Dikembalikan</p>
                                    @endif
                                    @if ($Order->status_bayar == 2)
                                        <p>Status Pembayaran : Sudah Melakukan Pembayaran</p>
                                    @endif
                                    @if ($Order->status_bayar == 4)
                                        <p>Status Pembayaran : Pembelian Dibatalkan</p>
                                    @endif
                                    @if ($Order->status_bayar == 5)
                                        <p>Status Pembayaran : Uang Telah Disetor ke Pemande</p>
                                    @endif
                                    @if ($Order->bukti_setor != null)
                                        <p>
                                            Bukti Setor Transfer : <br><img src="{{asset('images/backend_images/kebaya/medium/'.$Order->bukti_setor)}}" style="width:250px">
                                        </p>
                                    @endif
                                </div>
                            </div>
                            
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection