@extends('layouts.adminLayout.admin_design')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">kebaya</a> <a href="#" class="current">Tambah kebaya</a> </div>
        <h1>Tambah kebaya</h1>
        @if (session('flash_message_error'))
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{session('flash_message_error')}}</strong>
          </div>
        @endif
        @if (session('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{session('flash_message_success')}}</strong>
            </div>
        @endif
    </div>
    <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Form Tambah kebaya</h5>
            </div>
            <div class="widget-content nopadding">
              <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/tambah-kebaya')}}" name="add_kebaya" id="add_kebaya" novalidate="novalidate">
                {{ csrf_field() }}

                <div class="control-group">
                  <label class="control-label">Nama Kebaya</label>
                  <div class="controls">
                    <input type="text" name="namaKebaya" id="namaKebaya">
                  </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Dijahit Oleh</label>
                    <div class="controls">
                        <input type="text" name="dijahitOleh" id="dijahitOleh">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Dijahit Tanggal</label>
                    <div class="controls">
                        <input type="date" name="tanggalJahit" id="tanggalJahit">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Deskripsi Kebaya</label>
                    <div class="controls">
                        <textarea name="deskripsiGambar" id="deskripsiGambar"></textarea>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label">Gambar</label>
                    <div class="controls">
                      <input type="file" name="image" id="image">
                    </div>
                </div>
                <div class="form-actions">
                  <input type="submit" value="Tambah" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection