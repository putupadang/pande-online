@extends('layouts.adminLayout.admin_design')

@section('content')
<div id="content">
        <div id="content-header">
          <div id="breadcrumb"> <a href="{{url('admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{url('admin/setting')}}" class="current">Settings</a> </div>
          <h1>Admin Settings</h1>
          @if (session('flash_message_error'))
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{session('flash_message_error')}}</strong>
          </div>
          @endif
          @if (session('flash_message_success'))
              <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{session('flash_message_success')}}</strong>
              </div>
          @endif
        </div>
        <div class="container-fluid"><hr>
          <div class="row-fluid">
            <div class="row-fluid">
              <div class="span12">
                <div class="widget-box">
                  <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                    <h5>Ubah Password</h5>
                  </div>
                  <div class="widget-content nopadding">
                    <form class="form-horizontal" method="post" action="{{url('/admin/update-pwd')}}" name="password_validate" id="password_validate" novalidate="novalidate">
                      {{ csrf_field() }}
                    <div class="control-group">
                        <label class="control-label">Password Sekarang</label>
                        <div class="controls">
                            <input type="password" name="pwd_sekarang" id="pwd_sekarang" />
                            <span id="chkPwd"></span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Password baru</label>
                        <div class="controls">
                            <input type="password" name="pwd_baru" id="pwd_baru" />
                        </div>
                    </div>
                      <div class="control-group">
                        <label class="control-label">Konfirmasi Password</label>
                        <div class="controls">
                          <input type="password" name="pwd_konfir" id="pwd_konfir" />
                        </div>
                      </div>
                      <div class="form-actions">
                        <input type="submit" value="Ubah" class="btn btn-success">
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection