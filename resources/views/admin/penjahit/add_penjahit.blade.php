@extends('layouts.adminLayout.admin_design')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Penjahit</a> <a href="#" class="current">Tambah Penjahit</a> </div>
        <h1>Tambah Penjahit</h1>
        @if (session('flash_message_error'))
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{session('flash_message_error')}}</strong>
          </div>
        @endif
        @if (session('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{session('flash_message_success')}}</strong>
            </div>
        @endif
    </div>
    <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Form Tambah Penjahit</h5>
            </div>
            <div class="widget-content nopadding">
              <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/add-penjahit')}}" name="add_penjahit" id="add_penjahit" novalidate="novalidate">
                {{ csrf_field() }}

                <div class="control-group">
                  <label class="control-label">Nama Penjahit</label>
                  <div class="controls">
                    <input type="text" name="namaPenjahit" id="namaPenjahit">
                  </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Email</label>
                    <div class="controls">
                      <input type="email" name="emailPenjahit" id="emailPenjahit">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Password</label>
                    <div class="controls">
                      <input type="password" name="passwordPenjahit" id="passwordPenjahit">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Alamat</label>
                    <div class="controls">
                      <textarea name="alamat" id="alamat"></textarea>
                    </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Jenis Kelamin</label>
                  <div class="controls">
                    <select name="parent_id" id="parent_id" style="width: 220px">
                      <option value="0">Laki-Laki</option>
                      <option value="1">Perempuan</option>
                      {{-- @foreach ($levels as $level)
                        <option value="{{ $level->id }}">{{$level->nama}}</option>
                      @endforeach --}}
                    </select>
                  </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Mulai Jahit</label>
                    <div class="controls">
                      <input type="date" name="mulaiJahit" id="mulaiJahit">
                    </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">Pengalaman</label>
                  <div class="controls">
                    <textarea name="pengalaman" id="pengalaman"></textarea>
                  </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label">Gambar</label>
                    <div class="controls">
                      <input type="file" name="image" id="image">
                    </div>
                </div>
                <div class="form-actions">
                  <input type="submit" value="Tambah" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection