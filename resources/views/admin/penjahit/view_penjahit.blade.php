@extends('layouts.adminLayout.admin_design')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Pemande</a><a href="#" class="current">Lihat List Pemande</a> </div>
        <h1>List Pemande</h1>
        @if (session('flash_message_error'))
            <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{session('flash_message_error')}}</strong>
            </div>
        @endif
        @if (session('flash_message_success'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{session('flash_message_success')}}</strong>
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
            <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                <h5>List Pemande</h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered data-table">
                <thead>
                    <tr>
                    <th>Nama Pemande</th>
                    <th>Email Pemande</th>
                    <th>Alamat Pemande</th>
                    <th>Jenis Kelamin</th>
                    <th>Status</th>
                    <th>Pilihan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($penjahits as $penjahit)
                    
                        <tr class="gradeX">
                            <td>{{$penjahit->nama}}</td>
                            <td>{{$penjahit->email}}</td>
                            <td>{{$penjahit->alamat}}</td>
                            <td>{{$penjahit->jenis_kelamin}}</td>
                            @if ($penjahit->status == 0)
                                <td>Belum Terverifikasi</td>
                            @else
                                <td>Sudah Terverifikasi</td>
                            @endif
                            <td class="center">
                                @if ($penjahit->status == 0)
                                    <a href="#myModal{{$penjahit->id}}" data-toggle="modal" class="btn btn-info btn-mini">Detail</a> |
                                    {{-- <a href="{{url('pemande-detail/'.$penjahit->id)}}" class="btn btn-info btn-mini">Detail</a> |  --}}
                                    <a href=" {{url('admin/verifikasi-pemande/'.$penjahit->id)}} " class="btn btn-primary btn-mini">Verify</a> 
                                    {{-- <a href=" {{url('admin/hapus-pemande/'.$penjahit->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Delete</a> --}}
                                @else
                                    <a href="#myModal{{$penjahit->id}}" data-toggle="modal" class="btn btn-info btn-mini">Detail</a> |
                                    {{-- <a href="{{url('pemande-detail/'.$penjahit->id)}}" class="btn btn-info btn-mini">Detail</a> |  --}}
                                    <a href=" {{url('admin/verifikasi-pemande/'.$penjahit->id)}} " class="btn btn-primary btn-mini">Unferify</a> 
                                    {{-- <a href=" {{url('admin/hapus-pemande/'.$penjahit->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Delete</a> --}}
                                @endif
                                {{-- <a href="#myModal{{$penjahit->id}}" data-toggle="modal" class="btn btn-info btn-mini">Detail</a> | 
                                <a href=" {{url('admin/verifikasi-penjahit/'.$penjahit->id)}} " class="btn btn-primary btn-mini">Verify/Unferify</a> | 
                                <a href=" {{url('admin/hapus-penjahit/'.$penjahit->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Delete</a> --}}
                                {{-- <a rel="{{$penjahit->id}}" rel1="delete_penjahit" href="javascript:" class="btn btn-danger btn-mini deleteRecord">Delete</a> --}}
                            </td>
                        </tr>
                        
                        <div id="myModal{{$penjahit->id}}" class="modal hide">

                            <div class="modal-header">
                            <button data-dismiss="modal" class="close" type="button">×</button>
                            <h3>Detail Pemande {{$penjahit->nama}}</h3>
                            </div>
                            
                            <div class="modal-body">
                            <p>ID Pemande : {{$penjahit->id}}</p>
                            <p>Nama Pemande : {{$penjahit->nama}}</p>
                            <p>Email Pemande : {{$penjahit->email}}</p>
                            <p>Jenis Kelamin : {{$penjahit->jenis_kelamin}}</p>
                            <p>Alamat : {{$penjahit->alamat}}</p>
                            @if ($penjahit->status == 0)
                                <p>Status : Belum Terverifikasi</p>
                            @else
                                <p>Status : Sudah Terverifikasi</p>
                            @endif
                            <p>Foto Pemande : </p> <img src="{{asset('images/backend_images/kebaya/medium/'.$penjahit->gambar)}}" alt="" />
                            </div>
                        </div>
                        
                    @endforeach
                </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection