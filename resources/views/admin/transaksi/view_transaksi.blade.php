@extends('layouts.adminLayout.admin_design')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Transaksi</a><a href="#" class="current">Lihat List Transaksi</a> </div>
        <h1>List Transaksi</h1>
        @if (session('flash_message_error'))
            <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{session('flash_message_error')}}</strong>
            </div>
        @endif
        @if (session('flash_message_success'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{session('flash_message_success')}}</strong>
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                    <h5>List Transaksi</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table">
                        <thead>
                            <tr>
                            <th>Jenis Transaksi</th>
                            <th>Nama Pelanggan</th>
                            <th>Total Pembayaran</th>
                            <th>Tanggal Pembayaran</th>
                            <th>Bukti Pembayaran</th>
                            <th>Pilihan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($transaksis as $Transaksi)

                            @php
                            if ($Transaksi->jenis_transaksi == 0) {
                                $jenis_transaksi = "Transaksi Pemesanan";
                                $nama_pemesan = $Transaksi->pesanan->nama;
                                $no_hp = $Transaksi->pesanan->no_hp;
                            }
                            elseif ($Transaksi->jenis_transaksi == 1) {
                                $jenis_transaksi = "Transaksi Pembelian";
                                $nama_pemesan = $Transaksi->pembelian->nama_pembeli;
                                $no_hp = $Transaksi->pembelian->no_hp;
                            }
                            @endphp
                                <tr class="gradeX">
                                    <td>{{$jenis_transaksi}}</td>
                                    <td>{{$nama_pemesan}}</td>
                                    <td>{{$Transaksi->harga}}</td>
                                    <td>{{$Transaksi->created_at}}</td>
                                    <td>
                                        <img src="{{asset('images/frontend_images/transaksi/medium/'.$Transaksi->bukti_pembayaran)}}" alt="" style="width:100px">
                                    </td>
                                    
                                    <td class="center">
                                        @if ($Transaksi->status == 0)
                                            {{-- <a href="{{url('/penjahit-detail/'.$Transaksi->id_penjahit)}}"  class="btn btn-info btn-mini">Detail Penjahit</a> |  --}}
                                            {{-- <a href=" {{url('admin/verifikasi-Transaksi/'.$Transaksi->id)}} " class="btn btn-primary btn-mini">Sudah Lunas</a> |  --}}
                                            <a href="#myModal{{$Transaksi->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Transaksi</a> |
                                            <a href=" {{url('admin/hapus-Transaksi/'.$Transaksi->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Delete</a>
                                        @else
                                            {{-- <a href="{{url('/penjahit-detail/'.$Transaksi->id_penjahit)}}"  class="btn btn-info btn-mini">Detail Penjahit</a> |  --}}
                                            {{-- <a href=" {{url('admin/verifikasi-Transaksi/'.$Transaksi->id)}} " class="btn btn-primary btn-mini">Belum Lunas</a> |  --}}
                                            <a href="#myModal{{$Transaksi->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail Transaksi</a> |
                                            <a href=" {{url('admin/hapus-Transaksi/'.$Transaksi->id)}} " class="btn btn-danger btn-mini" id="deleteCategory">Delete</a>
                                        @endif
                                    </td>
                                </tr>
                                
                                <div id="myModal{{$Transaksi->id}}" class="modal hide">

                                    <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button">×</button>
                                    <h3>Detail Transaksi Pelanggan {{$nama_pemesan}}</h3>
                                    </div>
                                    
                                    <div class="modal-body">
                                        <p>ID Transaksi : {{$Transaksi->id}}</p>
                                        <p>Jenis Transaksi : {{$jenis_transaksi}}</p>
                                        <p>Nama Pelanggan : {{$nama_pemesan}}</p>
                                        <p>Nomor HP Pelanggan : {{$no_hp}}</p>
                                        <p>Total Harga : {{$Transaksi->harga}}</p>
                                        <p>Tanggal Transaksi : {{$Transaksi->created_at}}</p>
                                        <hr>
                                        <p>
                                            Bukti Pembayaran : <img src="{{asset('images/frontend_images/transaksi/medium/'.$Transaksi->bukti_pembayaran)}}" alt="" style="width:150px">
                                        </p>
                                    </div>
                                </div>
                                
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection