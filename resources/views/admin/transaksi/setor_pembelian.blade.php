@extends('layouts.adminLayout.admin_design')

@section('content')
<div id="content">
        <div id="content-header">
          <div id="breadcrumb"> <a href="{{url('admin/dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="{{url('admin/setting')}}" class="current">Setor Pembelian</a> </div>
          <h1>Setor Pembelian</h1>
          @if (session('flash_message_error'))
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{session('flash_message_error')}}</strong>
          </div>
          @endif
          @if (session('flash_message_success'))
              <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{session('flash_message_success')}}</strong>
              </div>
          @endif
        </div>
        <div class="container-fluid"><hr>
          <div class="row-fluid">
            <div class="row-fluid">
              <div class="span12">
                <div class="widget-box">
                  <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                    <h5>Setor Pembelian</h5>
                  </div>
                  <div class="widget-content nopadding">
                    <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/admin/setor-order-pembelian/'.$order->id)}}" name="password_validate" id="password_validate" novalidate="novalidate">
                      {{ csrf_field() }}
                      <div class="control-group">
                        <label class="control-label">Nama Pemande</label>
                        <div class="controls">
                            <input type="text" name="pwd_sekarang" id="pwd_sekarang" disabled value="{{$order->pemande->nama}}"/>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Total Harga</label>
                        <div class="controls">
                            <input type="text" name="pwd_sekarang" id="pwd_sekarang" disabled value="{{$order->galeri->harga}}"/>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Nomor Rekening</label>
                        <div class="controls">
                            <input type="text" name="pwd_sekarang" id="pwd_sekarang" disabled value="{{$order->pemande->no_rek}}"/>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Jenis Bank</label>
                        <div class="controls">
                            <input type="text" name="pwd_sekarang" id="pwd_sekarang" disabled value="{{$order->pemande->jenis_bank}}"/>
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label">Bukti Transfer</label>
                        <div class="controls">
                          <input type="file" name="image" id="image">
                        </div>
                      </div>
                      <div class="form-actions">
                        <input type="submit" value="Simpan" class="btn btn-success">
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection