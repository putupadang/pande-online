@extends('layouts.penjahitLayout.penjahit_design')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Galeri</a> <a href="#" class="current">Tambah Galeri</a> </div>
        <h1>Tambah Galeri</h1>

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{($error)}}</strong>    
                </div>
            @endforeach
        @endif
        
        @if (session('flash_message_error'))
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{session('flash_message_error')}}</strong>
          </div>
        @endif
        @if (session('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{session('flash_message_success')}}</strong>
            </div>
        @endif
    </div>
    <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Form Tambah Galeri</h5>
            </div>
            <div class="widget-content nopadding">
              <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/pemande/tambah-galeri')}}" name="add_Galeri" id="add_Galeri" novalidate="novalidate">
                {{ csrf_field() }}

                <input type="hidden" value="{{$user->id}}" name="idPenjahit">

                <div class="control-group">
                  <label class="control-label">Nama Barang</label>
                  <div class="controls">
                    <input type="text" name="namaBarang" id="namaBarang">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Harga</label>
                  <div class="controls">
                    <input type="number" name="harga" id="harga">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Stok Barang</label>
                  <div class="controls">
                    <input type="number" name="stok" id="stok">
                  </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Deskripsi Gambar</label>
                    <div class="controls">
                        <textarea name="deskripsiGambar" id="deskripsiGambar"></textarea>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label">Gambar 1</label>
                    <div class="controls">
                      <input type="file" name="image1" id="image1">
                    </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">Gambar 2</label>
                  <div class="controls">
                    <input type="file" name="image2" id="image2">
                  </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label">Gambar 3</label>
                  <div class="controls">
                    <input type="file" name="image3" id="image3">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Gambar 4</label>
                  <div class="controls">
                    <input type="file" name="image4" id="image4">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Gambar 5</label>
                  <div class="controls">
                    <input type="file" name="image5" id="image5">
                  </div>
                </div>

                <div class="form-actions">
                  <input type="submit" value="Tambah" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection