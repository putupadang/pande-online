@extends('layouts.penjahitLayout.penjahit_design')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Galeri</a><a href="#" class="current">Lihat List Galeri</a> </div>
        <h1>List Galeri {{$user->nama}} </h1>
        @if (session('flash_message_error'))
            <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{session('flash_message_error')}}</strong>
            </div>
        @endif
        @if (session('flash_message_success'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{session('flash_message_success')}}</strong>
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                    <h5>List Galeri</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                        <th>Nama Barang</th>
                        <th>Harga</th>
                        <th>Stok Barang</th>
                        <th>Deskripsi</th>
                        <th>Gambar</th>
                        <th>Pilihan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($galeri as $galeris)
                        
                            <tr class="gradeX">
                                <td>{{$galeris->nama_barang}}</td>
                                <td>{{$galeris->harga}}</td>
                                @if ($galeris->stok == 0)
                                    <td>Barang Terjual</td>
                                @else
                                    <td>{{$galeris->stok}}</td>
                                @endif
                                <td>{{$galeris->deskripsi}}</td>

                                <td>
                                    <img src="{{asset('images/backend_images/kebaya/medium/'.$galeris->gambar1)}}" alt="" style="width:100px">
                                </td>

                                <td class="center">
                                    <a href=" {{url('pemande/edit-galeri/'.$galeris->id)}} " class="btn btn-primary btn-mini">Edit</a> |
                                    <a href="{{url('pemande/hapus-galeri/'.$galeris->id)}}"  class="btn btn-danger btn-mini">Hapus</a> | 
                                    <a href="#myModal{{$galeris->id}}" data-toggle="modal" class="btn btn-success btn-mini">Detail</a>
                                </td>
                            </tr>
                            
                            <div id="myModal{{$galeris->id}}" class="modal hide">

                                <div class="modal-header">
                                <button data-dismiss="modal" class="close" type="button">×</button>
                                <h3>Detail Galeri {{$user->nama}}</h3>
                                </div>
                                
                                <div class="modal-body">
                                    <p>
                                        <img src="{{asset('images/backend_images/kebaya/large/'.$galeris->gambar1)}}">
                                    </p>
                                    <p>
                                        <img src="{{asset('images/backend_images/kebaya/large/'.$galeris->gambar2)}}">
                                    </p>
                                    <p>
                                        <img src="{{asset('images/backend_images/kebaya/large/'.$galeris->gambar3)}}">
                                    </p>
                                    <p>
                                        <img src="{{asset('images/backend_images/kebaya/large/'.$galeris->gambar4)}}">
                                    </p>
                                    <p>
                                        <img src="{{asset('images/backend_images/kebaya/large/'.$galeris->gambar5)}}">
                                    </p>
                                </div>
                            </div>
                            
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection