@extends('layouts.frontLayout.front_design')

@section('content')
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-1" style="margin-left:30px">
                    <div class="login-form"><!--login form-->
                        @if (session('flash_message_error'))
                        <div class="alert alert-error alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{session('flash_message_error')}}</strong>
                        </div>
                        @endif
                        @if (session('flash_message_success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{session('flash_message_success')}}</strong>
                            </div>
                        @endif
                        <h2>Form Login Pemande</h2>
                        <div class="signup-form"><!--sign up form-->
                            <form method="post" action="{{url('/pemande')}}" novalidate="novalidate" name="login_penjahit" id="login_penjahit">
                                {{ csrf_field() }}
    
                                <label class="control-label">Email</label>
                                <input type="email" id="email" name="email" placeholder="Masukkan Email"/>
    
                                <label class="control-label">Password</label>
                                <input type="password" id="password" name="password" placeholder="Masukkan Password"/>
    
                                <button type="submit" class="btn btn-default">Masuk</button>
                            </form>
                        </div>
                    </div>
                </div>

                {{-- <div class="col-sm-1" style="margin-left:100px">
                    <h2 class="or">Atau</h2>
                </div> --}}

                {{-- <div class="col-sm-4 col-sm-offset-1">
                    <div class="login-form"><!--login form-->
                        @if (session('flash_message_error'))
                        <div class="alert alert-error alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{session('flash_message_error')}}</strong>
                        </div>
                        @endif
                        @if (session('flash_message_success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{session('flash_message_success')}}</strong>
                            </div>
                        @endif
                        <h2>Form Login Pelanggan</h2>
                        <div class="signup-form"><!--sign up form-->
                            <form method="post" action="{{url('/login-pelanggan')}}" novalidate="novalidate" name="login_penjahit" id="login_penjahit">
                                {{ csrf_field() }}
    
                                <label class="control-label">Email</label>
                                <input type="email" id="email" name="email" placeholder="Masukkan Email"/>
    
                                <label class="control-label">Password</label>
                                <input type="password" id="password" name="password" placeholder="Masukkan Password"/>
    
                                <button type="submit" class="btn btn-default">Masuk</button>
                            </form>
                        </div>   
                    </div>
                </div> --}}

            </div>
        </div>
        <br><br>
@endsection