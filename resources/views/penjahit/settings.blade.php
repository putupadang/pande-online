@extends('layouts.penjahitLayout.penjahit_design')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Galeri</a> <a href="#" class="current">Update Biodata</a> </div>
        <h1>Update Biodata</h1>

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{($error)}}</strong>    
                </div>
            @endforeach
        @endif
        
        @if (session('flash_message_error'))
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{session('flash_message_error')}}</strong>
          </div>
        @endif
        @if (session('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{session('flash_message_success')}}</strong>
            </div>
        @endif
    </div>
    <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Form Ubah Biodata</h5>
            </div>
            <div class="widget-content nopadding">
              <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/pemande/setting/'.Auth::user()->id)}}" name="add_Galeri" id="add_Galeri" novalidate="novalidate">
                {{ csrf_field() }}

                <div class="control-group">
                  <label class="control-label">Nama</label>
                  <div class="controls">
                    <input type="text" name="nama" id="nama" value="{{$pemande->nama}}">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Nomor Hp</label>
                  <div class="controls">
                    <input type="text" name="no_hp" id="no_hp" value="{{$pemande->no_hp}}">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Nomor Rekening</label>
                  <div class="controls">
                    <input type="text" name="no_rek" id="no_rek" value="{{$pemande->no_rek}}">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Jenis Bank</label>
                  <div class="controls">
                    <input type="text" name="jenis_bank" id="jenis_bank" value="{{$pemande->jenis_bank}}">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Alamat</label>
                  <div class="controls">
                    <textarea name="alamat" id="alamat">{{$pemande->alamat}}</textarea>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Harga Jasa</label>
                  <div class="controls">
                    <input type="number" name="harga_jasa" id="harga_jasa" value="{{$pemande->harga_jasa}}">
                  </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label">Gambar</label>
                    <div class="controls">
                      <input type="file" name="gambar" id="gambar">
                    </div>
                </div>

                <div class="form-actions">
                  <input type="submit" value="Ubah" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection