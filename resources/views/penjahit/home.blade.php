@extends('layouts.penjahitLayout.penjahit_design')

@section('content')
    <!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
      <div id="content-header">
        <div id="breadcrumb"> <a hre ="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
      </div>
    <!--End-breadcrumbs-->
    <h1 style="margin:30px">Selamat Datang ! {{$user->nama}}</h1>
    <!--Action boxes-->
      <div class="container-fluid">
        <div class="quick-actions_homepage">
          <ul class="quick-actions">
            <li class="bg_lb"> <a href="/pemande/home"> <i class="icon-dashboard"></i> <span class="label label-important"></span> My Dashboard </a> </li>
            <li class="bg_ls"> <a href="/pemande/view-order"> <i class="icon-th"></i>Order Pesanan</a> </li>
            <li class="bg_lo"> <a href="/pemande/view-order-pande"> <i class="icon-th"></i>Order Pembelian</a> </li>
            <li class="bg_lg"> <a href="/pemande/lihat-galeri"> <i class="icon-th"></i>Galeri</a> </li>
          </ul>
        </div>
    <!--End-Action boxes-->    
    
<!--Chart-box-->    
    
<!--End-Chart-box--> 
    <hr/>
    <div class="row-fluid" style="margin-top: 14%">
      <h1 style="margin:30px">Total Penghasilan : Rp. {{$user->total_penghasilan}}</h1>        
    </div>
    </div>
</div>
<!--end-main-container-part-->
@endsection