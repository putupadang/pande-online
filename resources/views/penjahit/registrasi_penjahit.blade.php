@extends('layouts.frontLayout.front_design')

@section('content')
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-offset-1">
                    <div class="login-form"><!--login form-->

                        @if (count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-error alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{($error)}}</strong>    
                                </div>
                            @endforeach
                        @endif

                        @if (session('flash_message_error'))
                        <div class="alert alert-error alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{session('flash_message_error')}}</strong>
                        </div>
                        @endif
                        @if (session('flash_message_success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{session('flash_message_success')}}</strong>
                            </div>
                        @endif
                        <h2>Form Biodata Pemande</h2>
                        <form enctype="multipart/form-data" method="post" action="{{url('/registrasi-pemande')}}" novalidate="novalidate" name="registrasi" id="registrasi">
                            {{ csrf_field() }}

                            <div class="control-group">
                                <label class="control-label">Nama Pemande</label>
                                <div class="controls">
                                    <input type="text" name="namaPemande" id="namaPemande" required="required" placeholder="Nama Lengkap">
                                </div>
                            </div>
            
                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    <input type="email" name="emailPemande" id="emailPemande" required="required" placeholder="Email">
                                </div>
                            </div>
            
                            <div class="control-group">
                                <label class="control-label">Password</label>
                                <div class="controls">
                                    <input type="password" name="passwordPemande" id="passwordPemande" required="required" placeholder="Password">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">No. HP</label>
                                <div class="controls">
                                    <input type="number" name="noHP" id="noHP" required="required" placeholder="Nomor Hp">
                                </div>
                            </div>
            
                            <div class="control-group">
                                <label class="control-label">Alamat</label>
                                <div class="controls">
                                    <textarea name="alamat" id="alamat" required="required" placeholder="Alamat Rumah"></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">No. Rekening</label>
                                <div class="controls">
                                    <input type="number" name="no_rek" id="no_rek" required="required" placeholder="Nomor Rekening">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Jenis Bank</label>
                                <div class="controls">
                                    <input type="text" name="jenis_bank" id="jenis_bank" required="required" placeholder="Jenis Bank">
                                </div>
                            </div>
            
                            <div class="control-group"><br>
                                <label class="control-label">Jenis Kelamin</label>
                                <div class="controls">
                                    <span>
                                        <label>Laki-Laki<input type="radio" value="0" name="jenis_kelamin" id="jenis_kelamin"/></label>
                                        <label style="margin-left:20px">Perempuan<input type="radio" value="1" name="jenis_kelamin" id="jenis_kelamin"/></label><br>
                                    </span>
                                </div><br>
                            </div>

                            <div class="control-group"><br>
                                <label class="control-label">Gambar</label>
                                <div class="controls">
                                    <input type="file" name="gambar" id="gambar">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-default">Registrasi</button>
                        </form>
                    </div><!--/login form-->
                </div>
            </div>
        </div>
        <br><br>
@endsection