@extends('layouts.penjahitLayout.penjahit_design')

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Galeri</a> <a href="#" class="current">Tambah Galeri</a> </div>
        <h1>Tambah Galeri</h1>

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{($error)}}</strong>    
                </div>
            @endforeach
        @endif
        
        @if (session('flash_message_error'))
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{session('flash_message_error')}}</strong>
          </div>
        @endif
        @if (session('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{session('flash_message_success')}}</strong>
            </div>
        @endif
    </div>
    <div class="container-fluid"><hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Form Tambah Galeri</h5>
            </div>
            <div class="widget-content nopadding">
              <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{url('/pemande/terima-order/'.$order->id)}}">
                {{ csrf_field() }}

                <div class="control-group">
                  <label class="control-label">Nama Pemesan</label>
                  <div class="controls">
                    <input type="text" name="namaBarang" id="namaBarang" disabled value="{{$order->nama}}">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Deskripsi Pesanan</label>
                  <div class="controls">
                    <textarea name="" id="" cols="30" rows="10" disabled>{{$order->deskripsi_pesanan}}</textarea>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Lama Pengerjaan (hari)</label>
                  <div class="controls">
                    <input type="number" name="lama_hari" id="lama_hari" value="{{old('lama_hari')}}" required autofocus>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label">Harga</label>
                  <div class="controls">
                    <input type="number" name="harga" id="harga">
                  </div>
                </div>
                <div class="form-actions">
                  <input type="submit" value="Simpan" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection