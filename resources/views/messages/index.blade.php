@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h3>Online Users</h3><hr>

                <h5 id="no-online-users">No Online User</h5>
                
                <ul class="list-group" id="online-users">
                    
                </ul>
            </div>
            <div class="col-md-9 d-flex flex-column" style="height: 80vh;">
                <div class="h-100 mb-4 p-5" id="chat" style="overflow-y: scroll; background: white">

                    @foreach ($messages as $message)
                    
                    @if (auth()->user()->id == $message->user_id)
                    <div class="mt-4 w-50 text-white p-3 rounded float-right" style="background: orange">
                        <p>You</p><hr>
                        <p>{{$message->body}}</p>
                    </div>
                    <div class="clearfix"></div>
                    @else
                    <div class="mt-4 w-50 text-white p-3 rounded float-left" style="background: #0174DF">
                        <p>{{$message->user->name}}</p><hr>
                        <p>{{$message->body}}</p>
                    </div>
                    <div class="clearfix"></div>
                    @endif
                        {{-- <div class="mt-4 w-50 text-white p-3 rounded {{auth()->user()->id == $message->user_id ? 'float-right bg-primary' : 'float-left bg-success'}}">
                            <p>{{$message->user->name}}</p><hr>
                            <p>{{$message->body}}</p>
                        </div>
                        <div class="clearfix"></div> --}}
                    @endforeach

                </div>
                <form action="" class="d-flex">
                    <input type="text" name="" data-url="{{route('messages.store')}}" style="margin-right: 10px" class="form-control" id="chat-text">
                    <button class="btn btn-primary" type="submit">Kirim</button>
                </form>
            </div>
        </div>
    </div>
@endsection