@extends('layouts.frontLayout.front_design')

@section('content')
    {{-- <h1>nama penjahit : {{$penjahit->nama}}</h1>
    <h1>mulai menjahit sejak : {{$penjahit->mulaiJahit}}</h1> --}}

    <div class="container">
        <div class="row">
                        
            <div class="col-sm-12 padding-center">
                <div class="product-details"><!--product-details-->
                    <div class="col-sm-5">
                        <div class="view-product">
                            <img src="{{asset('images/backend_images/kebaya/medium/'.$penjahit->gambar)}}" alt="" />
                        </div>
                     
                    </div>
                    <div class="col-sm-7">
                        <div class="product-information"><!--/product-information-->
                            <img src="images/product-details/new.jpg" class="newarrival" alt="" />
                            <h2>{{$penjahit->nama}}</h2>
                            {{-- <p>Mulai Menjahit Sejak {{$penjahit->mulaiJahit}}</p> --}}
                            <img src="images/product-details/rating.png" alt="" />
                            <span>
                                <span>{{$penjahit->harga_jahit}}</span>
                            </span>
                            <p><b>No. Hp:</b> {{$penjahit->no_hp}}</p>
                            <p><b>Jenis Kelamin:</b> {{$penjahit->jenis_kelamin}}</p>
                            <p><b>Email:</b> {{$penjahit->email}}</p>
                            <p><b>Alamat:</b> {{$penjahit->alamat}}</p>
                            {{-- <p><b>Pengalaman:</b> {{$penjahit->pengalaman}}</p> --}}
                            <a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a>
                            <a href="{{url('/order/'.$penjahit->id)}}"><button type="button" class="btn btn-fefault cart">
                                <i class="fa fa-shopping-cart"></i>
                                Pesan
                            </button></a>
                        </div><!--/product-information-->
                    </div>
                </div><!--/product-details-->
                
                <div class="category-tab shop-details-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            {{-- <li class="active"><a href="#reviews" data-toggle="tab">Saran</a></li> --}}
                            {{-- <li><a href="#details" data-toggle="tab">Galeri Penjahit</a></li> --}}
                            <li class="active"><a href="#details" data-toggle="tab">Galeri Pemande</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="reviews" >
                            @foreach ($galeries as $galeri)
                            @if (!empty($galeri))
                            <div class="col-sm-3">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="{{asset('images/backend_images/kebaya/medium/'.$galeri->gambar1)}}" alt="" />
                                            <h2>{{$galeri->nama_baju}}</h2>
                                            <p>Harga : {{$galeri->harga}}</p>
                                            <p>Stok Barang : {{$galeri->stok}}</p>
                                            <a href="{{url('/barang-detail/'.$galeri->id)}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Beli</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                            <div class="col-sm-12">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <h2>Pemande Tidak Memiliki Galeri</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                        
                    </div>
                </div><!--/category-tab-->
                
            </div>
        </div>
    </div>
@endsection