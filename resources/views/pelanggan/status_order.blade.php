@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Status Order</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-condensed data-table">
                        <thead>
                            <tr class="text-center">
                                <th>Pesanan Atas Nama</th>
                                <th>Deskripsi Pesanan</th>
                                <th>Lama Pengerjaan</th>
                                <th>Status Pembayaran</th>
                                <th>Metode Pembayaran</th>
                                <th>Status Order</th>
                                <th>Detail Order</th>
                                {{-- <th>Invoice</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                            @php
                                $transaksi = App\Transaksi::where('id_order', $order->id)->first();                                
                            @endphp
                            <tr class="text-capitalize text-center">
                                <td>{{$order->nama}}</td>

                                <td>{{$order->deskripsi_pesanan}}</td>

                                <td>{{$order->lama_hari}} Hari</td>

                                @if ($order->status_bayar==0)
                                    <td>Anda Belum Melakukan Pembayaran</td>
                                @endif
                                @if ($order->status_bayar==1)
                                    <td>Menunggu Konfirmasi Pembayaran dari Admin</td>
                                @endif
                                @if ($order->status_bayar==2)
                                    <td>Pembayaran Telah di Kembalikan</td>
                                @endif
                                @if ($order->status_bayar==3)
                                    <td>Anda Telah Melakukan Pembayaran</td>
                                @endif
                                @if ($order->status_bayar==4)
                                    <td>Pesanan Anda Telah Dibatalkan</td>
                                @endif

                                @if (!empty($transaksi))
                                    @if ($transaksi->metode_transaksi==1)
                                        <td>Pembayaran Melalui Virtual Account</td>    
                                    @endif
                                    @if ($transaksi->metode_transaksi==0)
                                        <td>Pembayaran Melalui Manual Transfer</td>    
                                    @endif
                                @else
                                    <td>Belum Melakukan Pembayaran</td>
                                @endif
                                
                                @if ($order->status_order==0)
                                    <td>Menunggu Konfirmasi</td>
                                @endif
                                @if ($order->status_order==1)
                                    <td>Pesanan Sedang Proses</td>
                                @endif
                                @if ($order->status_order==2)
                                    <td>Pesanan Sudah Selesai</td>
                                @endif
                                @if ($order->status_order==3)
                                    <td>Pesanan di Tolak</td>
                                @endif
                                @if ($order->status_order==4)
                                    <td>Pesanan Anda Telah Dibatalkan</td>
                                @endif
                                @if ($order->status_order==5)
                                    <td>Barang Selesai Dibuat</td>
                                @endif
                                
                                <td><a href="#detailOrder{{$order->id}}" data-toggle="modal" class="btn btn-outline-info">Detail</a></td>
                                
                                {{-- @if ($order->status_order==2)
                                    <td>
                                        <a href="#myModal" data-toggle="modal" class="btn btn-outline-primary">Invoice</a>
                                    </td>
                                @else
                                    <td>Invoice Belum Dapat di Tampilkan</td>
                                @endif --}}
                            </tr>

                            <div id="myModal" class="modal hide bg-secondary text-center text-capitalize">
                                <div class="modal-header">
                                    <h1>Invoice</h1>
                                    <button data-dismiss="modal" class="close" type="button">close</button>
                                </div>
                                
                                <div class="modal-body">
                                    <h3></h3>
                                </div>
                            </div>

                            <div id="detailOrder{{$order->id}}" class="modal hide bg-secondary text-capitalize text-white-50">
                                <div class="modal-header">
                                    <h1>Detail Order</h1>
                                    <button data-dismiss="modal" class="close" type="button">close</button>
                                </div>
                                
                                <div class="modal-body">
                                    <h5>Tanggal Memesan : {{$order->created_at}}</h5>
                                    <h5>Nama Pemesan : {{$order->nama}}</h5>
                                    <h5>Email Pemesan : {{$order->email}}</h5>
                                    <h5>No. HP : {{$order->no_hp}}</h5>
                                    <h5>Alamat : {{$order->alamat}}</h5>
                                    <h5>Total Harga : {{$order->harga_total}}</h5>
                                    @if ($order->gambar_pesanan != null)
                                        <h5>Contoh Gambar 1 : </h5> <img src="{{asset('images/frontend_images/orders/small/'.$order->gambar_pesanan)}}" alt="">
                                    @endif
                                    @if ($order->gambar_pesanan1 != null)
                                        <h5>Contoh Gambar 2 : </h5> <img src="{{asset('images/frontend_images/orders/small/'.$order->gambar_pesanan1)}}" alt="">
                                    @endif
                                    @if ($order->gambar_pesanan2 != null)
                                        <h5>Contoh Gambar 3 : </h5> <img src="{{asset('images/frontend_images/orders/small/'.$order->gambar_pesanan2)}}" alt="">
                                    @endif
                                    @if ($order->gambar_pesanan3 != null)
                                        <h5>Contoh Gambar 4 : </h5> <img src="{{asset('images/frontend_images/orders/small/'.$order->gambar_pesanan3)}}" alt="">
                                    @endif
                                    @if ($order->gambar_pesanan4 != null)
                                        <h5>Contoh Gambar 5 : </h5> <img src="{{asset('images/frontend_images/orders/small/'.$order->gambar_pesanan4)}}" alt="">
                                    @endif
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
