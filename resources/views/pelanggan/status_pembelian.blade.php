@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Status Order</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-condensed data-table">
                        <thead>
                            <tr class="text-center">
                                <th>Pembelian Atas Nama</th>
                                <th>Status Pembayaran</th>
                                <th>Metode Pembayaran</th>
                                <th>Status Pembelian</th>
                                <th>Detail Pembelian</th>
                                {{-- <th>Invoice</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orderKebaya as $order)
                            {{-- @php
                                $transaksi = App\Transaksi::where('id_orderKebaya', $order->id)->first();
                                $galeri = App\GaleriPenjahit::where('id', $order->id_galeri)->first();
                                $penjahit = App\Penjahit::where('id', $order->id_penjahit)->first();
                            @endphp --}}
                            <tr class="text-capitalize text-center">
                                <td>{{$order->pelanggan->name}}</td>

                                @if ($order->status_bayar==0)
                                    <td>Anda Belum Melakukan Pembayaran</td>
                                @endif
                                @if ($order->status_bayar==1)
                                    <td>Menunggu Konfirmasi Pembayaran dari Admin</td>
                                @endif
                                @if ($order->status_bayar==3)
                                    <td>Pembayaran Telah di Kembalikan</td>
                                @endif
                                @if ($order->status_bayar==2)
                                    <td>Anda Telah Melakukan Pembayaran</td>
                                @endif
                                @if ($order->status_bayar==4)
                                    <td>Pembelian Dibatalkan</td>
                                @endif
                                @if ($order->status_bayar == 5)
                                    <td>Pembelian Sukses</td>
                                @endif

                                <td>Pembayaran Melalui Manual Transfer</td>
                                
                                @if ($order->status_terjual==0)
                                    <td>Menunggu Konfirmasi</td>
                                @endif
                                @if ($order->status_terjual==1)
                                    <td>Pembelian Barang Sukses</td>
                                @endif
                                @if ($order->status_terjual==2)
                                    <td>Pembelian Barang di Batalkan</td>
                                @endif
                                @if ($order->status_terjual==4)
                                    <td>Pembelian Barang di Batalkan</td>
                                @endif
                                
                                <td><a href="#detailOrder{{$order->id}}" data-toggle="modal" class="btn btn-outline-info">Detail</a></td>
                                
                                {{-- @if ($order->status_order==2)
                                    <td>
                                        <a href="#myModal" data-toggle="modal" class="btn btn-outline-primary">Invoice</a>
                                    </td>
                                @else
                                    <td>Invoice Belum Dapat di Tampilkan</td>
                                @endif --}}
                            </tr>

                            <div id="detailOrder{{$order->id}}" class="modal hide bg-secondary text-capitalize text-white-50">

                                <div class="modal-header">
                                    <h1>Detail Order</h1>
                                    <button data-dismiss="modal" class="close" type="button">close</button>
                                </div>
                                
                                <div class="modal-body">
                                    <h5>Tanggal Pembelian : {{$order->created_at}}</h5>
                                    <h5>Nama Pembeli : {{$order->nama_pembeli}}</h5>
                                    <h5>Email Pembeli : {{$order->email_pembeli}}</h5>
                                    <h5>No. HP : {{$order->no_hp}}</h5>
                                    <h5>Alamat : {{$order->alamat}}</h5>
                                    <h5>Pemilik Barang : {{$order->pemande->nama}}</h5>
                                    <h5>Total Harga : {{$order->galeri->harga}}</h5>
                                </div>

                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection