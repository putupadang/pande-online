<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class OrderKebaya extends Model
{
    protected $table = 'pembelian';

    public function galeri()
    {
        return $this->belongsTo('App\GaleriPenjahit', 'id_galeri', 'id');
    }
    public function pemande()
    {
        return $this->belongsTo('App\Penjahit', 'id_pemande', 'id');
    }
    public function pelanggan()
    {
        return $this->belongsTo('App\User', 'id_pelanggan', 'id');
    }
    // public $incrementing = false;

    // public static function boot()
    // {
    //     parent::boot();
    //     self::creating(function ($model) {
    //         $model->id = (string)Uuid::generate(4);
    //     });
    // }
}
