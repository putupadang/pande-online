<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class TransaksiVirtual extends Model
{
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(4);
        });
    }
}
