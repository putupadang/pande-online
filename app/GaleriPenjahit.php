<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class GaleriPenjahit extends Model
{
    // public $incrementing = false;

    // public static function boot()
    // {
    //     parent::boot();
    //     self::creating(function ($model) {
    //         $model->id = (string)Uuid::generate(4);
    //     });
    // }
    protected $table = 'galeri_pemande';
    public function pemande()
    {
        return $this->belongsTo('App\Penjahit', 'id_pemande', 'id');
    }
}
