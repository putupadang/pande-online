<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
            case 'pemande':
                if (Auth::guard($guard)->check()) {
                    return redirect('/pemande/home');
                }
                break;

            default:
                if (Auth::guard($guard)->check()) {
                    return redirect('admin/dashboard');
                }
                break;
        }

        // else {
        //     return redirect()->action('AdminController@login')->with('flash_message_error', 'Silakan Login terlebih dahulu !');
        // }

        return $next($request);
    }
}
