<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Session;
use App\Penjahit;
use App\Order;
use App\Transaksi;
use App\GaleriPenjahit;
use App\Message;
use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\OrderKebaya;
use Uuid;

class PenjahitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pemande');
    }

    public function homePage()
    {
        // $id = Penjahit::first();
        // dd($id);


        if (Auth::check()) {
            $user = Auth::user();
            return view('penjahit.home')->with(compact('user'));
        }
        // return view('penjahit.home');
    }

    public function settings(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {

            $request->validate([
                'nama' => 'required',
                'no_hp' => 'required',
                'alamat' => 'required',
                'harga_jasa' => 'required',
                'no_rek' => 'required',
                'jenis_bank' => 'required'
            ]);

            if ($request->hasFile('gambar')) {
                $image = Input::file('gambar');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    Penjahit::where('id', $id)->update(['gambar' => $filename]);
                }
            }

            Penjahit::where('id', $id)->update([
                'nama' => $request->nama,
                'no_hp' => $request->no_hp,
                'harga_jasa' => $request->harga_jasa,
                'no_rek' => $request->no_rek,
                'jenis_bank' => $request->jenis_bank
            ]);

            return redirect()->back()->with('flash_message_success', 'Sukses Mengupdate Biodata!!');
        }
        $pemande = Penjahit::where('id', $id)->first();
        return view('penjahit.settings', compact('pemande'));
    }

    public function viewOrder()
    {
        $user = Auth::user();
        $orders = Order::where('id_pemande', $user->id)->get();
        return view('penjahit.list_order')->with(compact('orders', 'user'));
    }

    public function terimaOrder($id = null, Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'harga' => 'required'
            ]);
            Order::where('id', $id)->update([
                'status_order' => '1',
                'harga_total' => $request->harga,
                'lama_hari' => $request->lama_hari
            ]);
            return redirect('pemande/view-order')->with('flash_message_success', 'Sukses Menerima Order !');
        }
        $order = Order::where('id', $id)->first();
        return view('penjahit.terima_order', compact('order'));
    }

    public function selesaiOrder($id = null)
    {
        $order = Order::where('id', $id)->first();
        // dd($order);
        $user = $order->id;

        if (($order->status_order) == '1') {
            Order::where(['id' => $id])->update([
                'status_order' => '2'
            ]);
            return redirect()->back()->with('flash_message_success', 'Order Telah Selesai !');
        }
    }

    public function tolakOrder($id = null)
    {
        $order = Order::where('id', $id)->first();
        // dd($order);
        $user = $order->id;

        if (($order->status_order) == '0') {
            Order::where(['id' => $id])->update([
                'status_order' => '3'
            ]);
            return redirect()->back()->with('flash_message_error', 'Order Telah Ditolak !');
        }
    }

    public function tambahGaleri(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'namaBarang' => 'required',
                'deskripsiGambar' => 'required',
                'harga' => 'required',
            ]);

            $data = $request->all();

            $penjahit = new GaleriPenjahit();
            $penjahit->id_pemande = Auth::user()->id;
            $penjahit->nama_barang = $data['namaBarang'];
            $penjahit->deskripsi = $data['deskripsiGambar'];
            $penjahit->harga = $data['harga'];
            $penjahit->stok = $data['stok'];

            //upload gambar
            if ($request->hasFile('image1')) {
                $image = Input::file('image1');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    $penjahit->gambar1 = $filename;
                }
            }

            if ($request->hasFile('image2')) {
                $image = Input::file('image2');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    $penjahit->gambar2 = $filename;
                }
            }

            if ($request->hasFile('image3')) {
                $image = Input::file('image3');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    $penjahit->gambar3 = $filename;
                }
            }

            if ($request->hasFile('image4')) {
                $image = Input::file('image4');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    $penjahit->gambar4 = $filename;
                }
            }

            if ($request->hasFile('image5')) {
                $image = Input::file('image5');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    $penjahit->gambar5 = $filename;
                }
            }

            $penjahit->save();

            // return redirect()->back()->with('flash_message_success', 'Sukses Menambah Galeri!!');
            return redirect('/pemande/lihat-galeri')->with('flash_message_success', 'Sukses menambah Galeri!');
        }

        $user = Auth::user();
        return view('penjahit.tambah_galeri')->with(compact('user'));
    }

    public function lihatGaleri($id = null)
    {
        $user = Auth::user();
        $galeri = GaleriPenjahit::where('id_pemande', $user->id)->get();

        return view('penjahit.lihat_galeri')->with(compact('galeri', 'user'));
    }

    public function editGaleri(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {

            $data = $request->all();

            if ($request->hasFile('image1')) {
                $image = Input::file('image1');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    GaleriPenjahit::where('id', $id)->update(['gambar1' => $filename]);
                }
            }

            if ($request->hasFile('image2')) {
                $image = Input::file('image2');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    GaleriPenjahit::where('id', $id)->update(['gambar2' => $filename]);
                }
            }

            if ($request->hasFile('image3')) {
                $image = Input::file('image3');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    GaleriPenjahit::where('id', $id)->update(['gambar3' => $filename]);
                }
            }

            if ($request->hasFile('image4')) {
                $image = Input::file('image4');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    GaleriPenjahit::where('id', $id)->update(['gambar4' => $filename]);
                }
            }

            if ($request->hasFile('image5')) {
                $image = Input::file('image5');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    GaleriPenjahit::where('id', $id)->update(['gambar5' => $filename]);
                }
            }

            GaleriPenjahit::where('id', $id)->update([
                'nama_barang' => $request->namaBarang,
                'deskripsi' => $request->deskripsiGambar,
                'harga' => $request->harga
            ]);

            return redirect('/pemande/lihat-galeri')->with('flash_message_success', 'Berhasil Mengubah Galeri!');
        }
        $galeri = GaleriPenjahit::where('id', $id)->first();
        return view('penjahit.edit_galeri')->with(compact('galeri'));
    }

    public function hapusGaleri($id = null)
    {
        GaleriPenjahit::where('id', $id)->delete();
        return redirect()->back()->with('flash_message_success', 'Sukses Menghapus Galeri!');
    }

    public function viewOrderKebaya()
    {
        $user = Auth::user();
        $orders = OrderKebaya::where('id_pemande', $user->id)->where('status_bayar', '!=', 0)->where('status_bayar', '!=', 1)->get();
        // $orders = OrderKebaya::get();
        return view('penjahit.list_order_kebaya')->with(compact('orders', 'user'));
    }

    public function terimaPembelian($id = null)
    {
        // $order = Order::where('id', $id)->first();
        // dd($order);
        // $user = $order->id;

        // if (($order->status_order) == '0') {
        //     Order::where('id', $id)->update([
        //         'status_order' => '1'
        //     ]);
        //     return redirect()->back()->with('flash_message_success', 'Sukses Menerima Order !');
        // }
        $jml_stok = OrderKebaya::where('id', $id)->first()->jml_stok;
        $stok_galeri = GaleriPenjahit::where('id', OrderKebaya::where('id', $id)->first()->id_galeri)->first()->stok;
        if ($stok_galeri - $jml_stok == 0) {
            OrderKebaya::where('id', $id)->update(['status_terjual' => '1']);
            GaleriPenjahit::where('id', OrderKebaya::where('id', $id)->first()->id_galeri)->update([
                'status_terjual' => '1',
                'stok' => $stok_galeri - $jml_stok
            ]);
        } else {
            OrderKebaya::where('id', $id)->update(['status_terjual' => '1']);
            GaleriPenjahit::where('id', OrderKebaya::where('id', $id)->first()->id_galeri)->update([
                'stok' => $stok_galeri - $jml_stok
            ]);
        }
        return redirect()->back()->with('flash_message_success', 'Sukses Menerima Order !');
    }

    public function tolakPembelian($id = null)
    {
        // $order = Order::where('id', $id)->first();
        // // dd($order);
        // $user = $order->id;

        // if (($order->status_order) == '0') {
        //     Order::where(['id' => $id])->update([
        //         'status_order' => '3'
        //     ]);
        //     return redirect()->back()->with('flash_message_error', 'Order Telah Ditolak !');
        // }
        OrderKebaya::where(['id' => $id])->update([
            'status_terjual' => '2'
        ]);
        return redirect()->back()->with('flash_message_error', 'Order Telah Ditolak !');
    }
}
