<?php

namespace App\Http\Controllers;

use Session;
use App\Order;
use App\Penjahit;
use App\Transaksi;
use App\OrderKebaya;
use App\GaleriPenjahit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('home')->with(compact('user'));
    }

    public function creditCard(Request $request, $id = null)
    {
        $data = $request->all();
        if ($request->isMethod('POST')) {
            // dd($data);

            //update status pembayaran di tabel order
            Order::where(['id' => $id])->update(['status_pembayaran' => '3']);

            //insert data transaksi
            $transaksi = new Transaksi();
            $transaksi->id_order = $data['idOrder'];
            $transaksi->total = $data['total'];
            $transaksi->exp_date = $data['expDate'];
            $transaksi->card_number = $data['cardNumber'];
            $transaksi->kode_cvc = $data['kodeCVC'];
            $transaksi->atas_nama = $data['atasNama'];
            $transaksi->metode_transaksi = 1;

            $transaksi->save();

            return redirect()->back()->with('flash_message_success', 'Sukses Melakukan Pembayaran!');
        }

        $order = Order::where(['id' => $id])->first();
        return view('transaksi.creditcard_payment')->with(compact('order'));
    }

    public function creditCardKebaya(Request $request, $id = null)
    {
        $data = $request->all();
        if ($request->isMethod('POST')) {

            //update status pembayaran di tabel order
            OrderKebaya::where(['id' => $id])->update(['status_pembayaran' => '2']);

            //insert data transaksi
            $transaksi = new Transaksi();
            $transaksi->id_orderKebaya = $data['idOrder'];
            $transaksi->total = $data['total'];
            $transaksi->exp_date = $data['expDate'];
            $transaksi->card_number = $data['cardNumber'];
            $transaksi->kode_cvc = $data['kodeCVC'];
            $transaksi->atas_nama = $data['atasNama'];
            $transaksi->metode_transaksi = 1;
            $transaksi->jenis_transaksi = 1;

            $transaksi->save();

            return redirect()->back()->with('flash_message_success', 'Sukses Melakukan Pembayaran!');
        }

        $order = OrderKebaya::where(['id' => $id])->first();
        return view('transaksi.creditcard_payment_kebaya')->with(compact('order'));
    }

    public function statusOrder($id = null)
    {
        $user = Auth::user();
        $orders = Order::where('id_pelanggan', $user->id)->get();
        return view('pelanggan.status_order')->with(compact('orders', 'user'));
    }

    public function statusPembelian($id = null)
    {
        $user = Auth::user();
        $orderKebaya = OrderKebaya::where('id_pelanggan', $user->id)->get();
        return view('pelanggan.status_pembelian')->with(compact('orderKebaya'));
    }

    public function invoice($id = null)
    {
        $user = Auth::user();
        $order = Order::where('id', $id)->first();

        return view('pelanggan.invoice');
    }

    public function invoiceKebaya($id = null)
    {
        $orderKebaya = OrderKebaya::where('id', $id)->first();

        if (!empty($orderKebaya)) {
            $galeri = GaleriPenjahit::where('id', $orderKebaya->id_galeri)->first();
        }
    }
}
