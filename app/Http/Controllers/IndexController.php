<?php

namespace App\Http\Controllers;

use App\Penjahit;
use App\GaleriPenjahit;
use App\ModelKebaya;
use Image;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;

class IndexController extends Controller
{
    public function index()
    {
        $penjahits = Penjahit::inRandomOrder()->take(9)->get();
        $models = Penjahit::inRandomOrder()->take(4)->get();
        // $kebayas = GaleriPenjahit::where('status_terjual', 0)
        //     ->orWhere('status_terjual', 0)
        //     ->orWhere('status_terjual', 2)
        //     ->orWhere('status_terjual', 4)
        //     ->inRandomOrder()->take(4)->get();
        // $kebayas = GaleriPenjahit::with(['pemande' => function ($q) {
        //     $q->where('status', 1);
        // }])->where('status_terjual', 0)->inRandomOrder()->take(4)->get();
        $barang = GaleriPenjahit::whereHas('pemande', function ($q) {
            $q->where('status', 1);
        })->where('status_terjual', 0)->inRandomOrder()->take(4)->get();
        return view('index')->with(compact('penjahits', 'models', 'barang'));
    }

    public function listPenjahit()
    {
        $penjahits = Penjahit::get();
        return view('penjahit_list')->with(compact('penjahits'));
    }

    public function detailPenjahit(Request $request, $id = null)
    {
        if (!empty($id)) {
            $penjahit = Penjahit::where(['id' => $id])->first();
            $galeries = GaleriPenjahit::where('id_pemande', $id)->where('status_terjual', 0)->inRandomOrder()->take(4)->get();

            return view('penjahit_detail')->with(compact('penjahit', 'galeries'));
        }
    }

    public function addPenjahit(Request $request)
    {
        if ($request->isMethod('post')) {

            $request->validate([
                'namaPemande' => 'required',
                'emailPemande' => 'required',
                'passwordPemande' => 'required',
                'alamat' => 'required',
                'noHP' => 'required',
                'gambar' => 'required',
                'no_rek' => 'required',
                'jenis_bank' => 'required'
            ]);

            $data = $request->all();

            $penjahit = new Penjahit();
            $penjahit->nama = $data['namaPemande'];
            $penjahit->email = $data['emailPemande'];
            $penjahit->password = bcrypt($data['passwordPemande']);
            $penjahit->alamat = $data['alamat'];
            $penjahit->no_hp = $data['noHP'];
            $penjahit->jenis_kelamin = $data['jenis_kelamin'] == '0' ? 'Laki-Laki' : 'Perempuan';
            $penjahit->no_rek = $request->no_rek;
            $penjahit->jenis_bank = $request->jenis_bank;
            // $penjahit->gambarKebaya = $data['image'];

            //upload gambar1
            if ($request->hasFile('gambar')) {
                $image = Input::file('gambar');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    $penjahit->gambar = $filename;
                }
            }

            $penjahit->save();


            return redirect()->back()->with('flash_message_success', 'Sukses Melakukan Registrasi! Silakan tunggu Verifikasi dari admin!!');


            // return redirect('/admin/view-penjahit')->with('flash_message_success', 'Sukses menambah Penjahit!');
        }

        return view('penjahit.registrasi_penjahit');
        // return view('admin.penjahit.add_penjahit');
    }

    public function listKebaya()
    {
        $galeris = GaleriPenjahit::whereHas('pemande', function ($q) {
            $q->where('status', 1);
        })->where('status_terjual', 0)->get();
        return view('galeri_kebaya.list_galeri')->with(compact('galeris'));
    }
}
