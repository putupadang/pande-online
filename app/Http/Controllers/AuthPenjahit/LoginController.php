<?php

namespace App\Http\Controllers\AuthPenjahit;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = 'pemande/home';

    public function __construct()
    {
        $this->middleware('guest:pemande')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('pemande');
    }

    public function loginPenjahit(Request $request)
    {
        if ($request->isMethod('post')) {

            $this->validateLogin($request);

            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }

            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        }
        return view('penjahit.login_penjahit');
    }

    public function logoutPenjahit(Request $request)
    {
        // Session::flush();
        // return redirect('/login')->with('flash_message_success', 'Penjahit Berhasil Logout !');

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }
}
