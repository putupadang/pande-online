<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Order;
use App\Penjahit;
use App\Transaksi;
use App\OrderKebaya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;

class TransaksiController extends Controller
{
    public function viewTransaksi(Request $request)
    {
        $data = $request->emailPemesan;

        if (!empty($data)) {
            // dd($data);
            // $transaksi = Order::where('nama_pemesan', $data)->get();
            $transaksi = Order::where([
                ['email', $data],
                ['status_bayar', 0]
            ])->get();

            $transaksiKebaya = OrderKebaya::where([
                ['email_pembeli', $data],
                ['status_bayar', 0]
            ])->get();
            // dd($transaksi);

            if ((!empty($transaksi)) || (!empty($transaksiKebaya))) {
                return view('transaksi.list_transaksi')->with(compact('transaksi', 'transaksiKebaya'));
            }

            // return view('transaksi.list_transaksi')->with(compact('transaksi', 'transaksiKebaya'));
        }
        return view('transaksi.view_transaksi');
    }

    public function viewTransaksiLogin(Request $request)
    {
        $pelanggan = Auth::user();
        // dd($pelanggan);

        if (!empty($pelanggan)) {
            $transaksi = Order::with('pelanggan', 'pemande')->where([
                ['id_pelanggan', $pelanggan->id],
                ['status_bayar', 0]
            ])->get();

            $transaksiKebaya = OrderKebaya::where([
                ['id_pelanggan', $pelanggan->id],
                ['status_bayar', 0]
            ])->get();

            if ((!empty($transaksi)) || (!empty($transaksiKebaya))) {
                return view('transaksi.list_transaksi')->with(compact('transaksi', 'transaksiKebaya'));
            }
        }
    }

    public function prosesTransaksi(Request $request, $id = null)
    {
        $data = $request->all();

        if ($request->isMethod('post')) {
            Order::where(['id' => $id])->update(['status_bayar' => '1']);

            //insert data transaksi
            $transaksi = new Transaksi();
            $transaksi->id_order = $data['idOrder'];
            $transaksi->harga = $data['harga'];
            $transaksi->jumlah = $data['jumlah'];
            $transaksi->jenis_transaksi = 0;

            //upload model kebaya
            if ($request->hasFile('buktiTransfer')) {
                $image = Input::file('buktiTransfer');

                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/frontend_images/transaksi/large/' . $filename;
                    $medium_image_path = 'images/frontend_images/transaksi/medium/' . $filename;
                    $small_image_path = 'images/frontend_images/transaksi/small/' . $filename;

                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);

                    //store image name in products tabel
                    $transaksi->bukti_pembayaran = $filename;
                }
            }

            $transaksi->save();

            return redirect()->back()->with('flash_message_success', 'Sukses Melakukan Pembayaran!');
        }

        $order = Order::with('pelanggan', 'pemande')->where(['id' => $id])->first();
        return view('transaksi.proses_transaksi')->with(compact('order'));
    }

    public function prosesTransaksiKebaya(Request $request, $id = null)
    {
        $data = $request->all();

        if ($request->isMethod('post')) {
            OrderKebaya::where(['id' => $id])->update(['status_bayar' => '1']);

            //insert data transaksi
            $transaksi = new Transaksi();
            $transaksi->id_order = $data['idOrder'];
            $transaksi->harga = $data['harga'];
            $transaksi->jumlah = $data['jumlah'];
            $transaksi->jenis_transaksi = 1;

            //upload model kebaya
            if ($request->hasFile('buktiTransfer')) {
                $image = Input::file('buktiTransfer');

                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/frontend_images/transaksi/large/' . $filename;
                    $medium_image_path = 'images/frontend_images/transaksi/medium/' . $filename;
                    $small_image_path = 'images/frontend_images/transaksi/small/' . $filename;

                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);

                    //store image name in products tabel
                    $transaksi->bukti_pembayaran = $filename;
                }
            }

            $transaksi->save();

            return redirect()->back()->with('flash_message_success', 'Sukses Melakukan Pembayaran!');
        }

        $order = OrderKebaya::where(['id' => $id])->first();
        return view('transaksi.proses_transaksi_kebaya')->with(compact('order'));
    }

    public function viewTransaksiAdmin()
    {
        // $transaksis = Transaksi::where('jenis_transaksi', 0)->get();
        $transaksis = Transaksi::get();
        // dd($transaksis);
        return view('admin.transaksi.view_transaksi')->with(compact('transaksis'));
    }

    public function viewTransaksiKebayaAdmin()
    {
        $transaksis = Transaksi::where('jenis_transaksi', 1)->get();
        // return view('admin.transaksi.view_transaksi')->with(compact('transaksis'));
    }

    public function creditCard(Request $request, $id = null)
    {
        return view('transaksi.creditcard_payment');
    }

    public function creditCardKebaya(Request $request, $id = null)
    {
        return view('transaksi.creditcard_payment');
    }
}
