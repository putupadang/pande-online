<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Order;
use App\Penjahit;
use App\OrderKebaya;
use App\GaleriPenjahit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;

class OrderController extends Controller
{
    public function addOrder(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            $request->validate([
                'namaPemesan' => 'required',
                'emailPemesan' => 'required',
                'noHP' => 'required',
                'alamat' => 'required'
            ]);

            $order = new Order();
            $order->id_pemande = $request->idPenjahit;
            $order->id_pelanggan = $request->idPelanggan;
            $order->harga_total = $data['hargaJahit'];
            $order->nama = $data['namaPemesan'];
            $order->email = $data['emailPemesan'];
            $order->no_hp = $data['noHP'];
            $order->alamat = $data['alamat'];
            // $order->no_hp = Auth::user()->no_hp;
            // $order->alamat = Auth::user()->alamat;
            $order->deskripsi_pesanan = $request->deskripsi_pesanan;

            //upload gambar
            if ($request->hasFile('gambar')) {
                $image = Input::file('gambar');

                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/frontend_images/orders/large/' . $filename;
                    $medium_image_path = 'images/frontend_images/orders/medium/' . $filename;
                    $small_image_path = 'images/frontend_images/orders/small/' . $filename;

                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);

                    //store image name in products tabel
                    $order->gambar_pesanan = $filename;
                }
            }

            if ($request->hasFile('gambar1')) {
                $image = Input::file('gambar1');

                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/frontend_images/orders/large/' . $filename;
                    $medium_image_path = 'images/frontend_images/orders/medium/' . $filename;
                    $small_image_path = 'images/frontend_images/orders/small/' . $filename;

                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);

                    //store image name in products tabel
                    $order->gambar_pesanan1 = $filename;
                }
            }

            if ($request->hasFile('gambar2')) {
                $image = Input::file('gambar2');

                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/frontend_images/orders/large/' . $filename;
                    $medium_image_path = 'images/frontend_images/orders/medium/' . $filename;
                    $small_image_path = 'images/frontend_images/orders/small/' . $filename;

                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);

                    //store image name in products tabel
                    $order->gambar_pesanan2 = $filename;
                }
            }

            if ($request->hasFile('gambar3')) {
                $image = Input::file('gambar3');

                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/frontend_images/orders/large/' . $filename;
                    $medium_image_path = 'images/frontend_images/orders/medium/' . $filename;
                    $small_image_path = 'images/frontend_images/orders/small/' . $filename;

                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);

                    //store image name in products tabel
                    $order->gambar_pesanan3 = $filename;
                }
            }

            if ($request->hasFile('gambar4')) {
                $image = Input::file('gambar4');

                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/frontend_images/orders/large/' . $filename;
                    $medium_image_path = 'images/frontend_images/orders/medium/' . $filename;
                    $small_image_path = 'images/frontend_images/orders/small/' . $filename;

                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);

                    //store image name in products tabel
                    $order->gambar_pesanan4 = $filename;
                }
            }

            $order->save();

            return redirect()->back()->with('flash_message_success', 'Sukses Melakukan Order!');
        }
        $penjahit = Penjahit::where(['id' => $id])->first();
        return view('order.tambah_order')->with(compact('penjahit'));
    }

    public function viewOrderAdmin()
    {
        $orders = Order::with('pemande', 'pelanggan')->get();
        return view('admin.order.view_order')->with(compact('orders'));
    }

    public function viewOrderPenjahit()
    {
        return view('penjahit.list_order');
    }

    public function detailKebaya(Request $request, $id = null) //TAMBAH ORDER KEBAYA SEGALIGUS
    {
        $galeri = GaleriPenjahit::where([
            ['id', $id],
            ['status_terjual', 0]
        ])->first();
        if ($request->isMethod('POST')) {
            // $data = $request->all();
            // dd($data);
            $request->validate([
                'idGaleri' => 'required',
                'idPenjahit' => 'required',
                'namaPembeli' => 'required',
                'noHP' => 'required',
                'alamat' => 'required',
            ]);

            $order = new OrderKebaya();
            $order->id_galeri = $request->idGaleri;
            $order->id_pemande = $request->idPenjahit;
            $order->id_pelanggan = $request->idPelanggan;
            $order->nama_pembeli = $request->namaPembeli;
            $order->email_pembeli = $request->emailPembeli;
            $order->no_hp = $request->noHP;
            $order->alamat = $request->alamat;
            $order->jml_stok = $request->jml_stok;
            $order->harga = $request->jml_stok * $galeri->harga;
            $order->save();

            return redirect()->back()->with('flash_message_success', 'Sukses Melakukan Order!');
        }
        return view('galeri_kebaya.detail_galeri')->with(compact('galeri'));
    }

    public function viewOrderKebayaAdmin()
    {
        $orders = OrderKebaya::with('galeri.pemande')->get();
        // dd($orders->galeri->pemande);
        return view('admin.order.view_orderKebaya')->with(compact('orders'));
    }

    public function batal($id = null)
    {
        $order = Order::where('id', $id)->first();
        Order::where(['id' => $id])->update([
            'status_bayar' => '4',
            'status_order' => '4'
        ]);
        return redirect()->back()->with('flash_message_success', 'Pesanan Telah Dibatalkan!');
    }

    public function batalBeli($id = null)
    {
        $order = Order::where('id', $id)->first();
        OrderKebaya::where(['id' => $id])->update([
            'status_bayar' => '4'
        ]);
        GaleriPenjahit::where(['id' => $id])->update([
            'status_terjual' => '4'
        ]);
        return redirect()->back()->with('flash_message_success', 'Pesanan Telah Dibatalkan!');
    }
}
