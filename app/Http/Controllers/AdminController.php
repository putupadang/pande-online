<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;
use App\Penjahit;
use App\Order;
use App\ModelKebaya;
use App\GaleriPenjahit;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Image;
use App\OrderKebaya;


class AdminController extends Controller
{
    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->input();
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'admin' => '1'])) {
                // echo ("success");
                // die;
                // Session::put('adminSession', $data['email']); -> cara lain membuat session
                // $request->session()->put('adminSession', $data['email']); buat session untuk lock route
                return redirect('/admin/dashboard');
            } else {
                // echo ("Failed");
                // die;
                return redirect('/admin')->with('flash_message_error', 'Email atau Password tidak valid !');
            }
        }

        return view('admin.admin_login');
    }

    public function dashboard()
    {
        // if (!session('adminSession')) {
        //     return redirect('/admin')->with('flash_message_error', 'Silakan Login terlebih dahulu !');
        // } -> cara lock route halaman

        // cara lain protect halaman menggunakan session
        // if (!Session::has('adminSession')) {
        //     return redirect('/admin')->with('flash_message_error', 'Silakan Login terlebih dahulu !');
        // }

        return view('admin.dashboard');
    }

    public function setting()
    {
        // if (!session('adminSession')) {
        //     return redirect('/admin')->with('flash_message_error', 'Silakan Login terlebih dahulu !');
        // }

        return view('admin.settings');
    }

    public function chkPassword(Request $request)
    {
        $data = $request->all();
        $password_sekarang = $data['pwd_sekarang'];
        $check_password = User::where(['admin' => '1'])->first();
        if (Hash::check($password_sekarang, $check_password->password)) {
            echo "true";
            die;
        } else {
            echo "false";
            die;
        }
    }

    public function updatePwd(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            $check_password = User::where(['email' => Auth::user()->email])->first();
            $password_sekarang = $data['pwd_sekarang'];
            if (Hash::check($password_sekarang, $check_password->password)) {
                $password = bcrypt($data['pwd_baru']);
                User::where('id', '1')->update(['password' => $password]);
                return redirect('/admin/setting')->with('flash_message_success', 'Sukses mengubah password!');
            } else {
                return redirect('/admin/setting')->with('flash_message_error', 'Password yang anda masukkan tidak sama!');
            }
        }
    }

    public function logout(Request $request)
    {
        // $request->session()->flush(); --> flush menggunakan variabel $request
        Session::flush();
        return redirect('/')->with('flash_message_success', 'Berhasil Logout !');
    }

    public function viewPenjahit()
    {
        $penjahits = Penjahit::get();
        // echo "<pre>";print_r($penjahits);die;

        return view('admin.penjahit.view_penjahit')->with(compact('penjahits'));
    }

    public function verifikasiPenjahit($id = null)
    {
        //get data dari tabel penjahit
        $penjahit = Penjahit::where(['id' => $id])->first();
        //get data dari kolom nama dari tabel penjahit
        $nama = $penjahit->nama;
        if (($penjahit->status) == '1') {
            Penjahit::where(['id' => $id])->update([
                'status' => '0'
            ]);
            return redirect()->back()->with('flash_message_error', 'Pemande ' . $nama . ' Telah di Unverify!');
        }

        if (!empty($id)) {
            Penjahit::where(['id' => $id])->update([
                'status' => '1'
            ]);
            return redirect()->back()->with('flash_message_success', 'Sukses Verifikasi Pemande ' . $nama . '!');
        }
    }

    public function delPemande($id = null)
    {
        if (!empty($id)) {
            Penjahit::where(['id' => $id])->delete();
            GaleriPenjahit::where('id_pemande', $id)->update([
                'status_terjual' => 2
            ]);
            return redirect()->back()->with('flash_message_success', 'Sukses Menghapus Pemande!');
        }
    }

    public function refundOrder($id = null)
    {
        $order = Order::where('id', $id)->first();
        if (($order->status_bayar) == '3') {
            Order::where(['id' => $id])->update([
                'status_bayar' => '2'
            ]);
            return redirect()->back()->with('flash_message_success', 'Pembayaran Dikembalikan ke Pelanggan!');
        }
    }

    public function setorPesanan(Request $request, $id = null)
    {
        $order = Order::where('id', $id)->first();
        if ($request->isMethod('post')) {
            $request->validate([
                'image' => 'required'
            ]);
            $extension = Input::file('image')->getClientOriginalExtension();
            $filename = rand(111, 99999) . '.' . $extension;
            $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
            $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
            $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
            //resize image
            Image::make(Input::file('image'))->save($large_image_path);
            Image::make(Input::file('image'))->resize(600, 600)->save($medium_image_path);
            Image::make(Input::file('image'))->resize(300, 300)->save($small_image_path);
            //store image name in products tabel
            Order::where(['id' => $id])->update(['status_order' => '5', 'bukti_setor' => $filename]);
            $penghasilan = Penjahit::where('id', $order->pemande->id)->first()->total_penghasilan;
            Penjahit::where('id', $order->pemande->id)->update([
                'total_penghasilan' => $penghasilan + $order->harga_total
            ]);
            return redirect('admin/order-admin')->with('flash_message_success', 'Uang di Kirim Ke Pemande!');
        }
        return view('admin.transaksi.setor_pemesanan', compact('order'));
    }

    public function setorPembelian(Request $request, $id = null)
    {
        $order = OrderKebaya::where('id', $id)->first();
        if ($request->isMethod('post')) {
            $request->validate([
                'image' => 'required'
            ]);
            $extension = Input::file('image')->getClientOriginalExtension();
            $filename = rand(111, 99999) . '.' . $extension;
            $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
            $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
            $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
            //resize image
            Image::make(Input::file('image'))->save($large_image_path);
            Image::make(Input::file('image'))->resize(600, 600)->save($medium_image_path);
            Image::make(Input::file('image'))->resize(300, 300)->save($small_image_path);
            //store image name in products tabel
            OrderKebaya::where('id', $id)->update(['status_bayar' => 5, 'bukti_setor' => $filename]);
            $penghasilan = Penjahit::where('id', $order->pemande->id)->first()->total_penghasilan;
            Penjahit::where('id', $order->pemande->id)->update([
                'total_penghasilan' => $penghasilan + $order->harga
            ]);
            return redirect('admin/order')->with('flash_message_success', 'Uang di Kirim Ke Pemande!');
        }
        return view('admin.transaksi.setor_pembelian', compact('order'));
    }

    public function lihatKebaya()
    {
        $galeri = ModelKebaya::get();
        return view('admin.model_kebaya.lihat_kebaya')->with(compact('galeri'));
    }

    public function tambahKebaya(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();

            $kebaya = new ModelKebaya();
            $kebaya->nama_kebaya = $data['namaKebaya'];
            $kebaya->dijahit_oleh = $data['dijahitOleh'];
            $kebaya->dijahit_tanggal = $data['tanggalJahit'];
            $kebaya->deskripsi = $data['deskripsiGambar'];

            //upload gambar
            if ($request->hasFile('image')) {
                $image = Input::file('image');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                    //store image name in products tabel
                    $kebaya->image = $filename;
                }
            }

            $kebaya->save();

            // return redirect()->back()->with('flash_message_success', 'Sukses Menambah Galeri!!');
            return redirect('/admin/lihat-kebaya')->with('flash_message_success', 'Sukses menambah Galeri!');
        }
        return view('admin.model_kebaya.tambah_kebaya');
    }

    public function editKebaya(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();

            if ($request->hasFile('image')) {
                $image = Input::file('image');
                if ($image->isValid()) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = rand(111, 99999) . '.' . $extension;
                    $large_image_path = 'images/backend_images/kebaya/large/' . $filename;
                    $medium_image_path = 'images/backend_images/kebaya/medium/' . $filename;
                    $small_image_path = 'images/backend_images/kebaya/small/' . $filename;
                    //resize image
                    Image::make($image)->save($large_image_path);
                    Image::make($image)->resize(600, 600)->save($medium_image_path);
                    Image::make($image)->resize(300, 300)->save($small_image_path);
                }
            } else {
                $filename = $data['image'];
            }

            ModelKebaya::where('id', $id)->update([
                'nama_kebaya' => $request->namaKebaya,
                'dijahit_oleh' => $request->dijahitOleh,
                'dijahit_tanggal' => $request->tanggalJahit,
                'deskripsi' => $request->deskripsiGambar,
                'image' => $filename
            ]);

            return redirect('/admin/lihat-kebaya')->with('flash_message_success', 'Berhasil Mengubah Galeri!');
        }
        $galeri = ModelKebaya::where('id', $id)->first();
        return view('admin.model_kebaya.edit_kebaya')->with(compact('galeri'));
    }

    public function hapusKebaya($id = null)
    {
        ModelKebaya::where('id', $id)->delete();
        return redirect()->back()->with('flash_message_success', 'Sukses Menghapus Galeri!');
    }

    public function verifikasiPembayaran($id = null)
    {
        $order = Order::where('id', $id)->first();
        Order::where(['id' => $id])->update(['status_bayar' => '3']);
        return redirect()->back()->with('flash_message_success', 'Sukses Verifikasi Pembayaran!');
    }

    public function refundOrderKebaya($id = null)
    {
        $order = OrderKebaya::where('id', $id)->first();

        OrderKebaya::where(['id' => $id])->update([
            'status_bayar' => '3'
        ]);
        return redirect()->back()->with('flash_message_success', 'Pembayaran Dikembalikan ke Pelanggan!');
    }

    public function verifikasiPembayaranKebaya($id = null)
    {
        $order = OrderKebaya::where('id', $id)->first();
        // GaleriPenjahit::where('id', $order->id_galeri)->update(['status_terjual' => '1']);
        // $galeri = GaleriPenjahit::where('id', $order->id_galeri)->first();
        // dd($galeri);
        OrderKebaya::where(['id' => $id])->update(['status_bayar' => '2']);

        return redirect()->back()->with('flash_message_success', 'Sukses Verifikasi Pembayaran!');
    }
}
