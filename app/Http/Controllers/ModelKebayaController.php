<?php

namespace App\Http\Controllers;

use App\ModelKebaya;
use Illuminate\Http\Request;

class ModelKebayaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelKebaya  $modelKebaya
     * @return \Illuminate\Http\Response
     */
    public function show(ModelKebaya $modelKebaya)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelKebaya  $modelKebaya
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelKebaya $modelKebaya)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelKebaya  $modelKebaya
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelKebaya $modelKebaya)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelKebaya  $modelKebaya
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelKebaya $modelKebaya)
    {
        //
    }
}
