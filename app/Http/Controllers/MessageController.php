<?php

namespace App\Http\Controllers;

use App\Message;
use App\User;
use Illuminate\Http\Request;
use App\Events\MessageDelivered;

class MessageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $messages = Message::all();
        // $nama = $messages->user->name;

        // $pesan = User::find(3)->messages()->where('body', 'halo')->first();
        // dd($pesan);

        return view('messages.index')->with(compact('messages'));
    }

    public function store(Request $request)
    {
        // return $request->all();
        $message = auth()->user()->messages()->create($request->all());
        broadcast(new MessageDelivered($message->load('user')))->toOthers();
    }

}
