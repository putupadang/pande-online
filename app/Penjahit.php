<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Uuid;

class Penjahit extends Authenticatable
{
    use Notifiable;
    // public $incrementing = false;
    protected $table = 'pemande';

    protected $fillable = [
        'nama', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'penjahit',
    ];

    // public static function boot()
    // {
    //     parent::boot();
    //     self::creating(function ($model) {
    //         $model->id = (string)Uuid::generate(4);
    //     });
    // }
    public function order()
    {
        return $this->hasMany('App\OrderKebaya', 'id_pemande', 'id');
    }

    public function orderPesanan()
    {
        return $this->hasMany('App\Order', 'id_pemande', 'id');
    }
}
