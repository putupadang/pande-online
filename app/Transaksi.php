<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuid;

class Transaksi extends Model
{
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string)Uuid::generate(4);
        });
    }

    public function pesanan()
    {
        return $this->belongsTo('App\Order', 'id_order', 'id');
    }

    public function pembelian()
    {
        return $this->belongsTo('App\OrderKebaya', 'id_order', 'id');
    }
}
