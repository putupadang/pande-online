
$(document).ready(function () {

	$("#pwd_baru").keyup(function () {
		var pwd_sekarang = $("#pwd_sekarang").val();
		$.ajax({
			type: 'get',
			url: '/admin/check-pwd',
			data: { pwd_sekarang: pwd_sekarang },
			success: function (resp) {
				if (resp == ("false")) {
					$("#chkPwd").html("<font color='red'>Password anda tidak sesuai!</font>");
				} else if (resp == ("true")) {
					$("#chkPwd").html("<font color='green'>Password anda sesuai!</font>");
				}
			}, error: function () {
				alert("Error");
			}
		});
	});

	$('input[type=checkbox],input[type=radio],input[type=file]').uniform();

	$('select').select2();

	// Form Validation
	$("#basic_validate").validate({
		rules: {
			required: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			date: {
				required: true,
				date: true
			},
			url: {
				required: true,
				url: true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight: function (element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	$("#number_validate").validate({
		rules: {
			min: {
				required: true,
				min: 10
			},
			max: {
				required: true,
				max: 24
			},
			number: {
				required: true,
				number: true
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight: function (element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});

	$("#password_validate").validate({
		rules: {
			pwd_sekarang: {
				required: true,
				minlength: 6,
				maxlength: 20
			},
			pwd_baru: {
				required: true,
				minlength: 6,
				maxlength: 20
			},
			pwd_konfir: {
				required: true,
				minlength: 6,
				maxlength: 20,
				equalTo: "#pwd_baru"
			}
		},
		errorClass: "help-inline",
		errorElement: "span",
		highlight: function (element, errorClass, validClass) {
			$(element).parents('.control-group').addClass('error');
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents('.control-group').removeClass('error');
			$(element).parents('.control-group').addClass('success');
		}
	});
});
