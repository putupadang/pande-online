<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            // $table->uuid('id')->primary();
            $table->integer('id_pemande');
            $table->integer('id_pelanggan')->nullable();
            $table->string('nama')->nullable();
            $table->string('email')->nullable();
            $table->string('no_hp');
            $table->string('alamat');
            $table->string('deskripsi_pesanan');
            $table->string('gambar_pesanan')->nullable();
            $table->string('gambar_pesanan1')->nullable();
            $table->string('gambar_pesanan2')->nullable();
            $table->string('gambar_pesanan3')->nullable();
            $table->string('gambar_pesanan4')->nullable();
            $table->string('lama_hari')->nullable();
            $table->double('harga_total')->nullable();
            $table->boolean('status_jenis_order')->default('0');
            $table->boolean('status_bayar')->default('0');
            $table->boolean('status_order')->default('0');
            $table->string('bukti_setor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
