<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembelian', function (Blueprint $table) {
            $table->increments('id');
            // $table->uuid('id')->primary();
            $table->integer('id_pemande');
            $table->integer('id_galeri');
            $table->integer('id_pelanggan')->nullable();
            $table->integer('jml_stok')->nullable();
            $table->string('nama_pembeli');
            $table->string('email_pembeli');
            $table->string('no_hp');
            $table->string('alamat');
            $table->integer('harga')->nullable();
            $table->boolean('status_bayar')->default('0');
            $table->boolean('status_perbaiki')->default('0');
            $table->boolean('status_terjual')->default('0');
            $table->string('bukti_setor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
