<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemandeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemande', function (Blueprint $table) {
            $table->increments('id');
            // $table->uuid('id')->primary();
            $table->string('nama');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('no_hp');
            $table->string('no_rek');
            $table->string('jenis_bank');
            $table->string('jenis_kelamin');
            $table->text('alamat');
            $table->string('gambar')->nullable();
            $table->boolean('status')->default('0');
            $table->double('total_penghasilan')->default('0');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penjahits');
    }
}
