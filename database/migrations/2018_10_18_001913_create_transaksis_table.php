<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            // $table->increments('id');
            $table->uuid('id')->primary();
            $table->string('id_order')->nullable();
            $table->string('id_galeri_pemande')->nullable();
            $table->integer('harga');
            $table->integer('jumlah');
            $table->string('bukti_pembayaran');
            $table->integer('jenis_transaksi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
