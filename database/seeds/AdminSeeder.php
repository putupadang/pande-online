<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Penjahit;
use Carbon\Carbon;
use App\GaleriPenjahit;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'name' => 'admin',
                'email' => 'admin@email.com',
                'password' => bcrypt('admin123'),
                'no_hp' => null,
                'alamat' => null,
                'admin' => 1
            ],
            [
                'name' => 'pelanggan',
                'email' => 'pelanggan@email.com',
                'password' => bcrypt('pelanggan123'),
                'no_hp' => '081999123456',
                'alamat' => 'denpasar',
                'admin' => 0
            ],
        ]);

        Penjahit::insert([
            [
                'nama' => 'pemande',
                'email' => 'pemande@email.com',
                'password' => bcrypt('pemande123'),
                'no_hp' => '081999213123',
                'jenis_kelamin' => 'Laki-Laki',
                'alamat' => 'jalan raya puputan',
                'gambar' => '52815.jpg',
                'status' => 1,
                'created_at' => Carbon::parse('2000-01-01'),
                'updated_at' => Carbon::parse('2000-01-01'),
                'jenis_bank' => 'BNI',
                'no_rek' => '321435225'
            ]
        ]);

        GaleriPenjahit::insert([
            [
                'id_pemande' => 1,
                'nama_barang' => 'pisau',
                'harga' => '15000',
                'gambar1' => '58241.jpg',
                'deskripsi' => 'pisau ini merupakan pisau yang dapat digunakan untuk kegiatan memasak',
                'status_terjual' => '0',
                'stok' => '2',
            ]
        ]);
    }
}
